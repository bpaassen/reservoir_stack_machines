#!/usr/bin/python3
"""
Tests the latch task

"""

# Copyright (C) 2020
# Alexander Schulz and Benjamin Paaßen
# Bielefeld University and The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Alexander Schulz and Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Alexander Schulz and Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'


import unittest
import numpy as np
#import esn
import rsm
from copy_task import generate_copy_seq
from copy_task import generate_stack_ops
import lmu




class TestLatch(unittest.TestCase):

    def test_data(self):

        # generate example sequences
        max_len = 5
        n = 2

        for r in range(10):

            X,Y = generate_copy_seq(max_len, n)

            # check the size
            self.assertEqual(Y.shape[0], X.shape[0]+1)
            self.assertEqual(X.shape[1], n+1)
            self.assertEqual(Y.shape[1], n)

            # check the content of X and Y.
            T = X.shape[0] // 2
            # X should be zero after T steps
            np.testing.assert_allclose(X[T+1:, :], np.zeros((T, n+1)))
            # and Y should be zero before
            np.testing.assert_allclose(Y[:T+1, :], np.zeros((T+1, n)))
            # Y after T should be a copy of X before T
            np.testing.assert_allclose(Y[T+1:, :], X[:T+1, :n])

            # generate the stack operations
            _, pops, pushs, shifts, _ = generate_stack_ops([X], [Y], max_len)

            # ensure the correct length
            self.assertEqual(Y.shape[0], len(pops[0]))
            self.assertEqual(Y.shape[0], len(pushs[0]))
            self.assertEqual(X.shape[0], len(shifts[0]))

            # and check that they work as expected

            # start with an empty stack
            stk = []
            Ypred = []
            for t in range(len(X)+1):

                # iterate over pop/push operations for the current time step
                for l in range(len(pops[0][t])):
                    # apply pop
                    for r in range(pops[0][t][l]):
                        stk.pop()
                    # apply push
                    stk.append(pushs[0][t][l] + n)

                # generate output; either as zero or as a copy of the
                # -max_len-1th element on the stack
                if len(stk) < max_len + 1 or isinstance(stk[-max_len-1], int):
                    Ypred.append(np.zeros(n))
                else:
                    Ypred.append(stk[-max_len-1][:n])

                # shift current input onto the stack
                if t < len(X):
                    stk.append(X[t, :])

            # stack output
            Ypred = np.stack(Ypred, 0)

            # compare predicted output to desired output
            np.testing.assert_allclose(Y, Ypred)


    def test_learning(self):
        # sample some data
        max_len = 10
        n = 8
        Xs = []
        Ys = []
        N = 150
        for i in range(N):
            X,Y = generate_copy_seq(max_len, n)
            Xs.append(X)
            Ys.append(Y)

        # train an RSM on it
        X, pop, push, shift, Y = generate_stack_ops(Xs, Ys, max_len)

        m = 300

        dim_in = n+1
        num_nonts = 1

        degree = m // (dim_in + num_nonts) - 1
        print('degree', degree)
        U, W   = lmu.initialize_reservoir(dim_in + num_nonts, degree, max_len*2)
        V = U[:, dim_in:]
        U = U[:, :dim_in]
        nonlin = lambda x : x
        model = rsm.RSM(U, V, W, nonlin = nonlin, C = 10000., classify_output = False) #, gamma=None

        model.fit(X, pop, push, shift, Y, verbose=True)

        N_test = 10
        errs = np.zeros(N_test)
        for i in range(N_test):
            X,Y = generate_copy_seq(max_len, n)

            _, pop, push, shift, _ = generate_stack_ops([X], [Y], max_len)

            Ypred = np.abs(np.round(model.predict(X, verbose = False)))

            self.assertEqual(Y.shape, Ypred.shape)

            errs[i] = np.mean(np.abs(Ypred - Y))

        self.assertTrue(np.mean(errs) < 1E-3)



if __name__ == '__main__':
    unittest.main()
