"""
Implements a GRU interface for easier use in our experimental pipeline.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import torch

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

class SimpleGRU(torch.nn.Module):
    """ A convenience API class for the GRU module.

    Attributes
    ----------
    dim_in: int
        The dimensionality of the input signal.
    dim_hid: int
        The number of recurrent neural network neurons.
    dim_out: int
        The dimensionality of the output.
    classify_output: bool (default = False)
        If set to True, dim_out will instead be treated as the number of
        possible classes and the loss will be crossentropy.

    """
    def __init__(self, dim_in, dim_hid, dim_out, classify_output = False):
        super(SimpleGRU, self).__init__()
        self.dim_in  = dim_in
        self.dim_hid = dim_hid
        self.dim_out = dim_out
        self.classify_output = classify_output
        self.gru_ = torch.nn.GRU(self.dim_in, self.dim_hid)
        self.out_ = torch.nn.Linear(self.dim_hid, self.dim_out)

    def forward_raw_(self, X):
        # concatenate a zero vector
        X = torch.cat((X, torch.zeros(1, self.dim_in)))
        # process with actual gru
        H, _ = self.gru_(X.unsqueeze(1))
        # remove batch dimension
        H = H[:, 0, :]
        # compute output
        Y = self.out_(H)
        return Y

    def forward(self, X):
        """ Convenience method to process an entire time series at once.

        Parameters
        ----------
        X: class torch.Tensor of size T x self.dim_in
            The input time series.
        temperature: float
            A temperature value for the underlying softmax function.

        Returns
        -------
        Y: class torch.Tensor of size T + 1 x self.dim_out
            The output time series.

        """
        Y = self.forward_raw_(X)

        if self.classify_output:
            Y = torch.argmax(Y, 1)

        return Y

    def compute_loss(self, X, Y):
        """ Convenience method to compute the loss for an input time series.

        Parameters
        ----------
        X: class torch.Tensor of size T x self.dim_in
            The input time series.
        Y: class torch.Tensor of size T + 1 x self.dim_out
            The desired output time series.

        Returns
        -------
        loss: class torch.Tensor
            The error between predicted and actual Y.

        """
        Ypred = self.forward_raw_(X)

        if self.classify_output:
            loss = torch.nn.functional.cross_entropy(Ypred, Y)
        else:
            loss = torch.nn.functional.mse_loss(Y, Ypred)
        return loss
