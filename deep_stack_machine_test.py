#!/usr/bin/python3
"""
Tests deep stack machines.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import torch
import numpy as np
import deep_stack_machine
import dyck_languages
import json_language
import cfg_utils
import rsm

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'


class TestDSM(unittest.TestCase):

    def test_training(self):
        # we try to learn the bracket grammar. First, set up a LR parser for it
        parser = dyck_languages.D1_PARSER

        # set up some training strings
        strings = ['()', '(())', '(())()()', '(()())']

        # convert it to training data for the RSM
        Xs, pops, pushs, shifts, Ys = rsm.lr_to_training_data(parser, strings)

        # initialize a DSM model
        model = deep_stack_machine.DeepStackMachine(dim_in = 2, dim_hid = 32, dim_out = 2, num_nonts = 1, max_pop = 3, classify_output = True)

        # initialize an optimizer
        optimizer = torch.optim.Adam(model.parameters(), lr = 2E-3, weight_decay = 1E-8)

        loss_avg = None

        for epoch in range(1000):
            optimizer.zero_grad()
            # sample an input string
            i = np.random.choice(len(Xs))
            # compute the loss
            loss = model.compute_loss(torch.tensor(Xs[i], dtype=torch.float), pops[i], pushs[i], shifts[i], torch.tensor(Ys[i], dtype=torch.long))

            # compute gradient
            loss.backward()

            # perform optimization step
            optimizer.step()

            if loss_avg is None:
                loss_avg = loss.item()
            else:
                loss_avg = 0.9 * loss_avg + 0.1 * loss.item()

            if (epoch + 1) % 100 == 0:
                print('loss avg after %d epochs: %g' % (epoch + 1, loss_avg))

        # check its performance on the training data
        for i in range(len(Xs)):
            Ypred = model.forward(torch.tensor(Xs[i], dtype=torch.float)).detach().numpy()
            np.testing.assert_almost_equal(Ys[i], Ypred, err_msg = 'problem for string %s' % strings[i])


    def test_rsm_training2(self):
        # test an example with the JSON language
        parser   = json_language.JSON_PARSER
        alphabet = ['n', 's', ',', ' ', '{', '}', 'k', ':', '[', ']']
        strings  = ['n', '[s]', '[n, [n, s], s]', '{k : [s, []], k : {k : n}}']

        # convert to training data for the RSM
        Xs, pops, pushs, shifts, Ys = rsm.lr_to_training_data(parser, strings, alphabet = alphabet)

        # initialize a DSM model
        model = deep_stack_machine.DeepStackMachine(dim_in = len(alphabet), dim_hid = 32, dim_out = 2, num_nonts = 3, max_pop = 8, classify_output = True)

        # initialize an optimizer
        optimizer = torch.optim.Adam(model.parameters(), lr = 2E-3, weight_decay = 1E-8)

        loss_avg = None

        for epoch in range(1000):
            optimizer.zero_grad()
            # sample an input string
            i = np.random.choice(len(Xs))
            # compute the loss
            loss = model.compute_loss(torch.tensor(Xs[i], dtype=torch.float), pops[i], pushs[i], shifts[i], torch.tensor(Ys[i], dtype=torch.long))

            # compute gradient
            loss.backward()

            # perform optimization step
            optimizer.step()

            if loss_avg is None:
                loss_avg = loss.item()
            else:
                loss_avg = 0.9 * loss_avg + 0.1 * loss.item()

            if (epoch + 1) % 100 == 0:
                print('loss avg after %d epochs: %g' % (epoch + 1, loss_avg))

        # check its performance on the training data
        for i in range(len(Xs)):
            Ypred = model.forward(torch.tensor(Xs[i], dtype=torch.float)).detach().numpy()
            np.testing.assert_almost_equal(Ys[i], Ypred, err_msg = 'problem for string %s' % strings[i])


if __name__ == '__main__':
    unittest.main()
