"""
Implements a tokenized version of the javascript object notation as
specified in https://www.json.org/json-en.html .

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import cfg_utils

# implement CFG for the (simplified) JSON language
JSON_GRAMMAR = cfg_utils.CFGGrammar({
        'V' : ['{}', '[]', '{O}', '[A]', 'n', 's'], # JSON value
        'O' : ['k : V, O', 'k : V'], # object content
        'A' : ['V, A', 'V'] # array content
    }, 'V')

# implement LR(1)-Parser for the palindrome language
JSON_PARSER = cfg_utils.LRParser([
            ('{}', '', 2, 'V'),
            ('[]', '', 2, 'V'),
            ('{O}', '', 3, 'V'),
            ('[A]', '', 3, 'V'),
            ('n', '', 1, 'V'),
            ('s', '', 1, 'V'),
            ('k : V, O', '', 8, 'O'),
            ('k : V', '}', 5, 'O'),
            ('V, A', '', 4, 'A'),
            ('V', ']', 1, 'A'),
        ], 'V')

