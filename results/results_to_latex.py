import numpy as np

models = ['rand-ESN', 'CRJ-ESN', 'LDN-ESN', 'GRU', 'SRNN', 'DSM', 'rand-RSM', 'CRJ-RSM', 'LDN-RSM']

esn_models     = [0, 1, 2]
rsm_models     = [6, 7, 8]
shallow_models = esn_models + rsm_models
deep_models    = [3, 4, 5]

datasets = ['latch', 'copy', 'repeat_copy', 'dyck1', 'dyck2', 'dyck3', 'anbn', 'palindrome', 'json']
dataset_labels = ['latch', 'copy', 'repeat copy', 'Dyck1', 'Dyck2', 'Dyck3', '$a^nb^n$', 'Palindrome', 'JSON']

ntm_datasets      = [0, 1, 2]
language_datasets = [3, 4, 5, 6, 7, 8]

R = 10

# load all data and put it into tensor
E = np.zeros((len(models), len(datasets), R))
T = np.zeros((len(models), len(datasets), R))

for data_idx in range(len(datasets)):
    E[shallow_models, data_idx, :] = np.loadtxt('%s_errors.csv' % datasets[data_idx], skiprows = 1).T
    E[deep_models, data_idx, :] = np.loadtxt('%s_deep_errors.csv' % datasets[data_idx], skiprows = 1).T
    T[shallow_models, data_idx, :] = np.loadtxt('%s_runtimes.csv' % datasets[data_idx], skiprows = 1).T
    T[deep_models, data_idx, :] = np.loadtxt('%s_deep_runtimes.csv' % datasets[data_idx], skiprows = 1).T

def print_table(data, data_idxs, boldface_low = True, shorten_deep = False):
    # format as LaTeX table with models as rows and datasets as columns
    print('\\begin{tabular}{l%s}' % ('c' * len(data_idxs)))
    header = 'model'
    for data_idx in data_idxs:
        header += ' & {%s}' % dataset_labels[data_idx]
    print(header + ' \\\\')
    for model_idx in range(len(models)):
        if model_idx == esn_models[0] or model_idx == rsm_models[0] or model_idx == deep_models[0]:
            print('\\cmidrule(lr){1-1} \\cmidrule(lr){2-%d}' % (1 + len(data_idxs)))
        row = models[model_idx]
        for data_idx in data_idxs:
            data_slice = data[model_idx, data_idx, :]
            if boldface_low and np.mean(data_slice) < 1E-2 and np.std(data_slice) < 1E-2:
                row += ' & $\\bm{%.2f \\pm %.2f}$' % (np.mean(data_slice), np.std(data_slice))
            elif shorten_deep and model_idx in deep_models:
                row += ' & $%.1f \\pm %.1f$' % (np.mean(data_slice), np.std(data_slice))
            else:
                row += ' & $%.2f \\pm %.2f$' % (np.mean(data_slice), np.std(data_slice))
        row += ' \\\\'
        print(row)
    print('\\end{tabular}')

# print errors for ntm datasets
print_table(E, ntm_datasets, True, False)
# print errors for language datasets
print_table(E, language_datasets, True, False)

# print runtimes for ntm datasets
print_table(T, ntm_datasets, False, True)
# print runtimes for language datasets
print_table(T, language_datasets, False, True)

# compute runtime factors:

ref_model_idx = models.index('LDN-RSM')

print('average runtime factor versus %s:' % models[ref_model_idx])
for model_idx in range(len(models)):
    if model_idx == ref_model_idx:
        continue
    print('%s: %g' % (models[model_idx], np.mean(T[model_idx, :, :] / T[ref_model_idx, :, :])))
