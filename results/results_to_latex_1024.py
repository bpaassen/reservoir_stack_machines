import numpy as np

models = ['rand-ESN', 'CRJ-ESN', 'LDN-ESN']

datasets = ['latch', 'copy', 'repeat_copy', 'dyck1', 'dyck2', 'dyck3', 'anbn', 'palindrome', 'json']
dataset_labels = ['latch', 'copy', 'repeat copy', 'Dyck1', 'Dyck2', 'Dyck3', '$a^nb^n$', 'Palindrome', 'JSON']

R = 10

# load all data and put it into tensor
E = np.zeros((len(models), len(datasets), R))
T = np.zeros((len(models), len(datasets), R))

for data_idx in range(len(datasets)):
    E[:, data_idx, :] = np.loadtxt('%s_1024_errors.csv' % datasets[data_idx], skiprows = 1).T
    T[:, data_idx, :] = np.loadtxt('%s_1024_runtimes.csv' % datasets[data_idx], skiprows = 1).T

def print_table(data, boldface_low = True):
    # format as LaTeX table with models as columns and datasets as rows
    print('\\begin{tabular}{l%s}' % ('c' * len(models)))
    header = 'dataset'
    for model_idx in range(len(models)):
        header += ' & {%s}' % models[model_idx]
    print(header + ' \\\\')
    print('\\cmidrule(lr){1-1} \\cmidrule(lr){2-%d}' % (1 + len(models)))
    for data_idx in range(len(datasets)):
        row = dataset_labels[data_idx]
        for model_idx in range(len(models)):
            data_slice = data[model_idx, data_idx, :]
            if boldface_low and np.mean(data_slice) < 1E-2 and np.std(data_slice) < 1E-2:
                row += ' & $\\bm{%.2f \\pm %.2f}$' % (np.mean(data_slice), np.std(data_slice))
            else:
                row += ' & $%.2f \\pm %.2f$' % (np.mean(data_slice), np.std(data_slice))
        row += ' \\\\'
        print(row)
    print('\\end{tabular}')

# print errors
print_table(E, True)
# print runtimes
print_table(T, False)
