import numpy as np

models = ['GRU', 'SRNN', 'DSM']

datasets = ['dyck1', 'dyck2', 'dyck3', 'anbn', 'palindrome', 'json']
dataset_labels = ['Dyck1', 'Dyck2', 'Dyck3', '$a^nb^n$', 'Palindrome', 'JSON']

R = 10

# load all data and put it into tensor
E = np.zeros((len(models), len(datasets), R))
T = np.zeros((len(models), len(datasets), R))

for data_idx in range(len(datasets)):
    E[:, data_idx, :] = np.loadtxt('%s_deep_errors.csv' % datasets[data_idx], skiprows = 1).T
    T[:, data_idx, :] = np.loadtxt('%s_deep_runtimes.csv' % datasets[data_idx], skiprows = 1).T

# format as LaTeX table with models as rows and datasets as columns
print('\\begin{tabular}{l%s}' % ('c' * len(datasets)))
header = 'model'
for dataset in dataset_labels:
    header += f' & {dataset}'
print(header + ' \\\\')
print('\\cmidrule(lr){1-1} \\cmidrule(lr){2-%d}' % (1 + len(datasets)))
for model_idx in range(len(models)):
    row = models[model_idx]
    for data_idx in range(len(datasets)):
        errs = E[model_idx, :, data_idx]
        row += ' & $%.2f \\pm %.2f$' % (np.mean(errs), np.std(errs))
    row += ' \\\\'
    print(row)
print('\\end{tabular}')


# format runtime
print('\\begin{tabular}{l%s}' % ('c' * len(datasets)))
header = 'model'
for dataset in dataset_labels:
    header += f' & {dataset}'
print(header + ' \\\\')
print('\\cmidrule(lr){1-1} \\cmidrule(lr){2-%d}' % (1 + len(datasets)))
for model_idx in range(len(models)):
    row = models[model_idx]
    for data_idx in range(len(datasets)):
        runtimes = T[model_idx, :, data_idx]
        row += ' & $%.1f \\pm %.1f$' % (np.mean(runtimes), np.std(runtimes))
    row += ' \\\\'
    print(row)
print('\\end{tabular}')
