"""
Implements the palindromic language over {a, b}

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import cfg_utils

# implement CFG for the palindrome language with $ in the center
PALINDROME_GRAMMAR = cfg_utils.CFGGrammar({'S' : ['aSa', 'bSb', '$']}, 'S')

# implement LR(1)-Parser for the palindrome language
PALINDROME_PARSER = cfg_utils.LRParser([
            ('aSa', '', 3, 'S'),
            ('bSb', '', 3, 'S'),
            ('$', '', 1, 'S')
        ], 'S')

