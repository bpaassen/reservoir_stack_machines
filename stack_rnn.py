"""
This code provides an interface to the stack RNN model provided by
Suzgun et al. (2019). Memory-Augmented Recurrent Neural Networks Can Learn
Generalized Dyck Languages

IMPORTANT: Because the authors did not provide a license to their code, we do
not enclose their code in our distribution. Please download the rnn_models.py
code file at
https://github.com/suzgunmirac/marnns/blob/master/models/rnn_models.py
and put it into this folder.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import torch
import rnn_models

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

class StackRNN(rnn_models.SRNN_Softmax):
    """ A convenience API class for the SRNN_Softmax model provided by
    Suzgun et al. (2019). The model combines a vanilla RNN with a
    differentiable stack. In their experiments, this was the overall best
    performing model, so this is our baseline.

    Attributes
    ----------
    dim_in: int
        The dimensionality of the input signal.
    dim_hid: int
        The number of recurrent neural network neurons.
    dim_out: int
        The dimensionality of the output.
    memory_size: int (default = 104)
        The maximum size of the stack. Default value is taken from the
        original implementation.
    memory_dim: int (default = 5)
        The dimensionality of the stack. Default value is taken from the
        original implementation.

    """
    def __init__(self, dim_in, dim_hid, dim_out, memory_size = 104, memory_dim = 5):
        super(StackRNN, self).__init__(hidden_dim = dim_hid, output_size = dim_out, vocab_size = dim_in, memory_size = memory_size, memory_dim = memory_dim)
        self.forward_iteration_ = super(StackRNN, self).forward

    def forward(self, X, temperature = 1.):
        """ Convenience method to process an entire time series at once.

        Parameters
        ----------
        X: class torch.Tensor of size T x self.dim_in
            The input time series.
        temperature: float
            A temperature value for the underlying softmax function.

        Returns
        -------
        Y: class torch.Tensor of size T + 1 x self.dim_out
            The output time series.

        """
        # initialize hidden state
        hidden = self.init_hidden()
        # initialize stack
        memory = torch.zeros(self.memory_size, self.memory_dim)
        # initialize output time series
        Y = torch.zeros(X.shape[0]+1, self.output_size)
        for t in range(X.shape[0]):
            # process all input symbols
            Y[t, :], hidden, memory = self.forward_iteration_(X[t, :].view(1, 1, -1), hidden, memory, temperature)
        # do one final step
        Y[-1, :], _, _ = self.forward_iteration_(torch.zeros(1, 1, self.vocab_size), hidden, memory, temperature)
        return Y

    def compute_loss(self, X, Y, temperature = 1.):
        """ Convenience method to compute the loss for an input time series.

        Parameters
        ----------
        X: class torch.Tensor of size T x self.dim_in
            The input time series.
        Y: class torch.Tensor of size T + 1 x self.dim_out
            The desired output time series.
        temperature: float
            A temperature value for the underlying softmax function.

        Returns
        -------
        loss: class torch.Tensor
            The mean square error between predicted and actual Y.

        """
        Ypred = self.forward(X, temperature)
        return torch.nn.functional.mse_loss(Y, Ypred)

