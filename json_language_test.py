#!/usr/bin/python3
"""
Tests the JSON language implementation.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import numpy as np
import json_language

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'


class TestJSONLanguage(unittest.TestCase):

    def test_parsing(self):
        # test a trivial example
        example = 'n'
        rules, y = json_language.JSON_PARSER.parse(example)
        rules_expected = [[], [('n', '', 1, 'V')]]
        self.assertEqual(rules_expected, rules)
        y_expected = np.array([0, 1])
        np.testing.assert_almost_equal(y_expected, y)

        # test a more complex example
        example = '{k : [[], [n], [n, n]], k : s}'
        rules, y = json_language.JSON_PARSER.parse(example)

        rules_expected = [
            # we expect that the first 7 characters can not be parsed directly
            [], [], [], [], [], [], [], [],
            # then, we finally have [], which we can parse into V
            [('[]', '', 2, 'V')],
            # next, we can parse n into V and V into A
            [], [], [], [('n', '', 1, 'V'), ('V', ']', 1, 'A')],
            # next, we can parse [A] into V
            [('[A]', '', 3, 'V')],
            # next, we can parse n into V twice
            [], [], [], [('n', '', 1, 'V')],
            # and the second time we also parse V into A and V, A into A
            [], [], [('n', '', 1, 'V'), ('V', ']', 1, 'A'), ('V, A', '', 4, 'A')],
            # next, we can parse [A] into V, V into A, and V, A into A
            [('[A]', '', 3, 'V'), ('V', ']', 1, 'A'), ('V, A', '', 4, 'A'), ('V, A', '', 4, 'A')],
            # next, we can parse [A] into V
            [('[A]', '', 3, 'V')],
            # next, we can parse s into V, k : V into O, and k : V, O into O
            [], [], [], [], [], [], [('s', '', 1, 'V'), ('k : V', '}', 5, 'O'), ('k : V, O', '', 8, 'O')],
            # finally, we can parse {O} into V and complete the parse
            [('{O}', '', 3, 'V')]
        ]
        self.assertEqual(rules_expected, rules)

        y_expected = np.zeros(len(example) + 1)
        y_expected[-1] = 1

    def test_sampling(self):
        # sample some elements from the JSON language and ensure that the
        # parsing works

        ps = {
            'V' : [0.1, 0.1, 0.2, 0.2, 0.2, 0.2],
            'O' : [0.4, 0.6],
            'A' : [0.4, 0.6]
        }

        N = 100

        lens = np.zeros(N)

        for i in range(N):
            w = json_language.JSON_GRAMMAR.sample_word(ps = ps)
            _, y = json_language.JSON_PARSER.parse(w)

            lens[i] = len(w)

            self.assertTrue(len(y) >= 1)
            self.assertTrue(y[-1] > 0.5, 'word %s was not accepted by the JSON grammar' % w)

if __name__ == '__main__':
    unittest.main()
