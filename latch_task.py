"""
Implements latch task.

"""

# Copyright (C) 2020
# Alexander Schulz and Benjamin Paaßen
# Bielefeld University and The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Alexander Schulz and Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Alexander Schulz and Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import random
import numpy as np
import cfg_utils


def generate_latch_seq(max_len, num_switches):
	# start by sampling the sequence length
	T = random.randrange(num_switches * 3, max_len)
	# initialize the input and output sequence
	X = np.zeros((T, 1))#*0.5 #X = np.zeros((T, 1))
	#X(0,:) = 1
	Y = np.zeros((T+1, 1)) # add one element to Y for the RSM implementation
	# sample the switching points
	t_last = 0
	# add 1 pop for each input 0 with a delay
	pop = [[0]] 
	for j in range(num_switches):
		lo = int(T / num_switches) * j + int(0.25 * T / num_switches)
		hi = int(T / num_switches) * (j+1) - int(0.25 * T / num_switches)
		t  = random.randrange(lo, hi)
		X[t, :] = 1.
		Y[t_last:t] = j % 2
		pop = pop + [[1]]*(t-t_last-min(j,1)) + [[0]]
		t_last = t
	Y[t_last:-1] = num_switches % 2
	pop = pop + [[1]]*(T-t_last-1)
	#Y[-1] = Y[-2]
	
	# define also apropriate actions for pop, push, shift
	push = [[0] for i in range(T+1)]
	#shift = X # only push input to the stack if its a '1'
	shift = np.ones((T, 1))
	return X, Y, pop, push, shift

LATCH_GRAMMAR = cfg_utils.CFGGrammar({'S' : ['S0', 'A1'], 'A' : ['A0', 'S1', '0']}, 'S')
LATCH_PARSER  = cfg_utils.LRParser([
	('S0', '', 2, 'S'),
	('A1', '', 2, 'S'),
	('A0', '', 2, 'A'),
	('S1', '', 2, 'A'),
	('0', '', 1, 'A')], 'S')
















