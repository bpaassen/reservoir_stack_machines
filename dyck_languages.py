"""
Implements Dyck languages, i.e. balanced bracket languages, as described
in Suzgun et al. (2019). Memory-Augmented Recurrent Neural Networks
Can Learn Generalized Dyck Languages.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

from collections import deque
import numpy as np
import cfg_utils

# implement CFG for the D1 language (one type of bracket)
D1_GRAMMAR = cfg_utils.CFGGrammar({'S' : ['SS', '(S)', '()']}, 'S')

# implement CFG for the D2 language (two types of brackets)
D2_GRAMMAR = cfg_utils.CFGGrammar({'S' : ['SS', '(S)', '[S]', '()', '[]']}, 'S')

# implement CFG for the D3 language (three types of brackets)
D3_GRAMMAR = cfg_utils.CFGGrammar({'S' : ['SS', '(S)', '[S]', '{S}', '()', '[]', '{}']}, 'S')

DYCK_GRAMMARS = [D1_GRAMMAR, D2_GRAMMAR, D3_GRAMMAR]

# implement LR(1)-Parser for the D1 language (one type of bracket)
D1_PARSER = cfg_utils.LRParser([
            ('SS', '', 2, 'S'),
            ('(S)', '', 3, 'S'),
            ('(', ')', 0, 'S')
        ], 'S')

# implement LR(1)-Parser for the D2 language (two types of brackets)
D2_PARSER = cfg_utils.LRParser([
            ('SS', '', 2, 'S'),
            ('(S)', '', 3, 'S'),
            ('[S]', '', 3, 'S'),
            ('(', ')', 0, 'S'),
            ('[', ']', 0, 'S')
        ], 'S')

# implement LR(1)-Parser for the D3 language (three types of brackets)
D3_PARSER = cfg_utils.LRParser([
            ('SS', '', 2, 'S'),
            ('(S)', '', 3, 'S'),
            ('[S]', '', 3, 'S'),
            ('{S}', '', 3, 'S'),
            ('(', ')', 0, 'S'),
            ('[', ']', 0, 'S'),
            ('{', '}', 0, 'S')
        ], 'S')

DYCK_PARSERS = [D1_PARSER, D2_PARSER, D3_PARSER]
