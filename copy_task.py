"""
Implements copy task.

"""

# Copyright (C) 2020
# Alexander Schulz and Benjamin Paaßen
# Bielefeld University and The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Alexander Schulz and Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Alexander Schulz and Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import random
import numpy as np
import cfg_utils


def generate_copy_seq(max_len, n, force_length = False):
     # start by sampling the sequence length
    if force_length:
        T = max_len
    else:
        T = random.randrange(1, max_len+1)
    # initialize the input and output sequence
    X = np.zeros((2*T+1, n+1))
    Y = np.zeros((2*T+2, n)) # output should be one element longer
    # fill the input sequence with random bits
    X[0:T, :n] = np.round(np.random.rand(T, n))
    # write -1 in the feature of the eos 
    X[0:T, n] = -1*np.ones(T)
    # set an end-of-sequence marker
    X[T, n] = 1.
    # copy the input to the output
    Y[T+1:-1, :] = X[0:T, :n]
    return X, Y


# given sequences, generate the pop, push and shift actions
def generate_stack_ops(seqsIn, seqsOut, max_len):
    
    #Xs = []
    #Ys = []
    pops = []
    pushs = []
    shifts = []
    
    for n in range(len(seqsIn)):
        seqIn = seqsIn[n]
        #seqOun = seqsOut[n]
        T = int((seqIn.shape[0]-1)/2)
        
        shift = np.ones(seqIn.shape[0])
        # push after the eos symbol nonterminals until stack has max_len length
        push = [[]]*T + [[1] * (max_len-T)] + [[]]*(T+1)
        pop = [[]]*T + [[0] * (max_len-T)] + [[]]*(T+1)

        # add an additional dimension for the non-temrinal
        # is handled inside rsm
        #X = np.zeros((2*T+1, n+2))
        #X[:,:n+1] = X
        #Y = np.zeros((2*T+1, n+2))
        #Y[:,:n] = seqOun
        
        shifts.append(shift)
        pushs.append(push)
        pops.append(pop)
    
    return seqsIn, pops, pushs, shifts, seqsOut
    















