#!/usr/bin/python3
"""
Tests the latch task

"""

# Copyright (C) 2020
# Alexander Schulz and Benjamin Paaßen
# Bielefeld University and The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Alexander Schulz and Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Alexander Schulz and Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'


import unittest
import numpy as np
#import esn
import rsm
from repeat_copy_task import generate_repeat_copy_seq
from repeat_copy_task import generate_stack_ops_rc
import lmu




class TestRepeatCopy(unittest.TestCase):

    def test_data(self):

        # generate example sequences
        max_repeats = 3
        max_len = 5
        n = 2

        for r in range(10):

            X,Y = generate_repeat_copy_seq(max_len, max_repeats, n)
            #print(X)
            #print(Y)

            # check the size
            self.assertEqual(Y.shape[0], X.shape[0]+1)
            self.assertEqual(X.shape[1], n+1)
            self.assertEqual(Y.shape[1], n)
            
            # generate the stack operations
            _, pops, pushs, shifts, _ = generate_stack_ops_rc([X], [Y], max_len)
            #print(pops)
            #print(pushs)

            # ensure the correct length
            self.assertEqual(Y.shape[0], len(pops[0]))
            self.assertEqual(Y.shape[0], len(pushs[0]))
            self.assertEqual(X.shape[0], len(shifts[0]))


            

    def test_learning(self):
        # sample some data
        max_repeats = 10
        max_len = 10
        n = 8
        Xs = []
        Ys = []
        N = 100
        for i in range(N):
            X,Y = generate_repeat_copy_seq(max_len, max_repeats, n)
            Xs.append(X)
            Ys.append(Y)

        # train an RSM on it
        X, pop, push, shift, Y = generate_stack_ops_rc(Xs, Ys, max_len)

        m = 256

        dim_in = n+1
        num_nonts = 1

        degree = m // (dim_in + num_nonts) - 1
        print('degree', degree)
        U, W   = lmu.initialize_reservoir(dim_in + num_nonts, degree, max_len*2)
        V = U[:, dim_in:]
        U = U[:, :dim_in]
        nonlin = lambda x : x
        model = rsm.RSM(U, V, W, nonlin = nonlin, classify_output = False) #, gamma=None

        model.fit(X, pop, push, shift, Y, verbose=True)

        N_test = 10
        errs = np.zeros(N_test)
        for i in range(N_test):
            X,Y = generate_repeat_copy_seq(max_len, max_repeats*2, n)

            _, pop, push, shift, _ = generate_stack_ops_rc([X], [Y], max_len)

            Ypred = np.abs(np.round(model.predict(X, verbose = False)))

            self.assertEqual(Y.shape, Ypred.shape)

            errs[i] = np.mean(np.abs(Ypred - Y))

        self.assertTrue(np.mean(errs) < 1E-3)
        


if __name__ == '__main__':
    unittest.main()
