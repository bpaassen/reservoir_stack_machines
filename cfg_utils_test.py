#!/usr/bin/python3
"""
Tests the CFG utils.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import numpy as np
import cfg_utils

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

class TestCFG(unittest.TestCase):

    def assertRulesEqual(self, rexp, rpred, rules):
        self.assertEqual(len(rexp), len(rpred))
        for t in range(len(rexp)):
            self.assertEqual(len(rexp[t]), len(rpred[t]), 'expected rules %s at time step %d but got %s' % (str(rexp[t]), t, str(rpred[t])))
            for j in range(len(rexp[t])):
                self.assertEqual(len(rexp[t]), len(rpred[t]), 'expected rule %s as %dth rule in time step %d but got %s' % (str(rexp[t][j]), t, j, str(rpred[t][j])))
                self.assertEqual(rules[rexp[t][j]], rpred[t][j])

    def test_fsm(self):
        # set up a simple LR parser for the regular language (aa)*b
        rules = [
            ('Aa', '', 2, 'B'),
            ('Ba', '', 2, 'A'),
            ('Ab', '', 2, 'C'),
            ('#', '', 0, 'A')
        ]
        parser = cfg_utils.LRParser(rules, 'C')

        # test a few words
        rpred, ypred = parser.parse('a')
        self.assertRulesEqual([[3], [0]], rpred, rules)
        self.assertEqual([0, 0], ypred)

        rpred, ypred = parser.parse('b')
        self.assertRulesEqual([[3], [2]], rpred, rules)
        self.assertEqual([0, 1], ypred)

        rpred, ypred = parser.parse('ab')
        self.assertRulesEqual([[3], [0], []], rpred, rules)
        self.assertEqual([0, 0, 0], ypred)

        rpred, ypred = parser.parse('aab')
        self.assertRulesEqual([[3], [0], [1], [2]], rpred, rules)
        self.assertEqual([0, 0, 0, 1], ypred)

    def test_bracket_grammar(self):
        # set up a simple LR parser for a bracket language
        rules = [
            ('SS', '', 2, 'S'),
            ('(S)', '', 3, 'S'),
            ('(', ')', 0, 'S')
        ]
        parser = cfg_utils.LRParser(rules, 'S')

        # test a few words
        rpred, ypred = parser.parse('()')
        self.assertRulesEqual([[], [2], [1]], rpred, rules)
        self.assertEqual([0, 0, 1], ypred)

        rpred, ypred = parser.parse('(())')
        self.assertRulesEqual([[], [], [2], [1], [1]], rpred, rules)
        self.assertEqual([0, 0, 0, 0, 1], ypred)

        rpred, ypred = parser.parse('()()')
        self.assertRulesEqual([[], [2], [1], [2], [1, 0]], rpred, rules)
        self.assertEqual([0, 0, 1, 0, 1], ypred)

        rpred, ypred = parser.parse('(())()')
        self.assertRulesEqual([[], [], [2], [1], [1], [2], [1, 0]], rpred, rules)
        self.assertEqual([0, 0, 0, 0, 1, 0, 1], ypred)


    def test_palindrome(self):
        # set up a simple LR parser for the palindrome language over ab
        # with special center symbol
        rules = [
            ('$', '', 1, 'S'),
            ('aSa', '', 3, 'S'),
            ('bSb', '', 3, 'S')
        ]
        parser = cfg_utils.LRParser(rules, 'S')

        # test a few words
        rpred, ypred = parser.parse('$')
        self.assertRulesEqual([[], [0]], rpred, rules)
        self.assertEqual([0, 1], ypred)

        rpred, ypred = parser.parse('a$a')
        self.assertRulesEqual([[], [], [0], [1]], rpred, rules)
        self.assertEqual([0, 0, 0, 1], ypred)

        rpred, ypred = parser.parse('ab$bb')
        self.assertRulesEqual([[], [], [], [0], [2], []], rpred, rules)
        self.assertEqual([0, 0, 0, 0, 0, 0], ypred)

        rpred, ypred = parser.parse('ab$ba')
        self.assertRulesEqual([[], [], [], [0], [2], [1]], rpred, rules)
        self.assertEqual([0, 0, 0, 0, 0, 1], ypred)


    def test_anbn(self):
        # set up a simple LR parser for the language a^nb^n
        rules = [
            ('a', 'b', 0, 'S'),
            ('aSb', '', 3, 'S')
        ]
        parser = cfg_utils.LRParser(rules, 'S')

        # test a few words
        rpred, ypred = parser.parse('')
        self.assertRulesEqual([[]], rpred, rules)
        self.assertEqual([0], ypred)

        rpred, ypred = parser.parse('ab')
        self.assertRulesEqual([[], [0], [1]], rpred, rules)
        self.assertEqual([0, 0, 1], ypred)

        rpred, ypred = parser.parse('aab')
        self.assertRulesEqual([[], [], [0], [1]], rpred, rules)
        self.assertEqual([0, 0, 0, 0], ypred)

        rpred, ypred = parser.parse('aabb')
        self.assertRulesEqual([[], [], [0], [1], [1]], rpred, rules)
        self.assertEqual([0, 0, 0, 0, 1], ypred)

    def test_sampling(self):
        # set up a grammar for a^nb^n
        grammar = cfg_utils.CFGGrammar({ 'S' : ['ab', 'aSb'] }, 'S')

        # set up a simple LR parser for the language a^nb^n
        rules = [
            ('a', 'b', 0, 'S'),
            ('aSb', '', 3, 'S')
        ]
        parser = cfg_utils.LRParser(rules, 'S')

        # ensure that every randomly sampled word parses correctly
        for i in range(100):
            w = grammar.sample_word()
            _, y = parser.parse(w)

            self.assertTrue(len(y) >= 1)
            self.assertTrue(y[-1] > 0.5, 'word %s was not accepted' % w)

        # do the same test again with a grammar for JSON arrays
        grammar = cfg_utils.CFGGrammar({ 'S' : ['[A]'], 'A' : ['E,A', 'E'], 'E' : ['1', '2', '3'] }, 'S')

        # set up a simple LR parser for the language
        rules = [
            ('[A]', '', 3, 'S'),
            ('E,A', '', 3, 'A'),
            ('E', ']', 1, 'A'),
            ('1', '', 1, 'E'),
            ('2', '', 1, 'E'),
            ('3', '', 1, 'E')
        ]
        parser = cfg_utils.LRParser(rules, 'S')

        N = 100
        lens = np.zeros(N)

        for i in range(N):
            w = grammar.sample_word(ps = {'A' : [0.8, 0.2]})
            _, y = parser.parse(w)

            lens[i] = len(w)

            self.assertTrue(len(y) >= 1)
            self.assertTrue(y[-1] > 0.5, 'word %s was not accepted' % w)

if __name__ == '__main__':
    unittest.main()
