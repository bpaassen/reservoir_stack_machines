#!/usr/bin/python3
"""
Tests reservoir stack machines.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import numpy as np
import rsm
import lmu
import json_language
from sklearn.base import BaseEstimator, ClassifierMixin
from scipy.sparse import csr_matrix
import cfg_utils

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

class LinearBinaryClassifier(BaseEstimator, ClassifierMixin):

    def __init__(self, W, b, disjunction = False):
        self.W = W
        self.b = b
        self.disjunction = disjunction

    def fit(self, X, Y):
        raise ValueError('not implemented.')

    def predict(self, X):
        y = np.dot(X, self.W.T) + self.b
        y = np.argmax(y)
        if self.disjunction and y > 0:
            return [1]
        else:
            return [y]


class TestRSM(unittest.TestCase):

    def test_brackets_manual(self):
        # test whether an RSM with manually set parameters is able to
        # correctly parse the bracket language generated from the rules
        # S -> SS | (S) | epsilon

        # the solution approach is to have the state store the past two
        # symbols, namely (, ), or S in one-hot coding, yielding a
        # six-dimensional binary vector.

        # set up coding matrix for input symbols
        U = np.zeros((6, 2))
        U[0, 0] = 1.
        U[1, 1] = 1.
        # set up coding matrix for nonterminal symbols
        V = np.zeros((6, 1))
        V[2, 0] = 1.
        # set up reservoir matrix
        W = np.zeros((6, 6))
        W[3, 0] = 1.
        W[4, 1] = 1.
        W[5, 2] = 1.
        W = csr_matrix(W)

        # initialize a RSM
        model = rsm.RSM(U, V, W)

        # set up the pop classifier; we should pop 2 symbols
        # if SS is on the stack and we should pop 3 symbols if
        # S) is on the stack. Otherwise, we pop nothing
        Wpop = np.zeros((4, 12))
        Wpop[2, 6 + 3 - 1] = 1.
        Wpop[2, 6 + 6 - 1] = 1.
        Wpop[3, 6 + 2 - 1] = 1.
        Wpop[3, 6 + 6 - 1] = 1.
        bpop = np.zeros(4)
        bpop[1:] = -1.5
        model.c_pop_ = LinearBinaryClassifier(Wpop, bpop)

        # set up the push classifier; we should push S if
        # SS is on the stack, S) is on the stack, or if ( is
        # on the stack and ) is on the input
        Wpush = np.zeros((4, 12))
        Wpush[1, 6 + 3 - 1] = 1.
        Wpush[1, 6 + 6 - 1] = 1.
        Wpush[2, 6 + 2 - 1] = 1.
        Wpush[2, 6 + 6 - 1] = 1.
        Wpush[3, 1] = 1.
        Wpush[3, 6] = 1.
        bpush = np.zeros(4)
        bpush[1:] = -1.5
        model.c_push_ = LinearBinaryClassifier(Wpush, bpush, True)

        # set up the shift classifier; we should shift if there is input left
        Wshift = np.zeros((2, 12))
        Wshift[1, :2] = 1.
        bshift = np.array([0., -0.5])
        model.c_shift_ = LinearBinaryClassifier(Wshift, bshift)

        # set up the output model. We want to return 1 if there is only S
        # on the stack
        Wout = np.zeros((2, 12))
        Wout[1, 6 + 3 - 1] = 1.
        Wout[1, 9:] = -1.
        bout = np.array([0., -0.5])
        model.c_out_ = LinearBinaryClassifier(Wout, bout)


        # set up a few example strings
        Xs = ['()', '()()', '(())', '(())()']
        Ys = [np.array([0., 0., 1.]), np.array([0., 0., 1., 0., 1.]), np.array([0., 0., 0., 0., 1.]), np.array([0., 0., 0., 0., 1., 0., 1.])]

        for i in range(len(Xs)):
            # code string as one-hot-coding
            x = Xs[i]
            X = np.zeros((len(x), 2))
            for t in range(len(x)):
                if x[t] == '(':
                    X[t, 0] = 1.
                else:
                    X[t, 1] = 1.
            # feed into RSM
            Ypred = model.predict(X)
            # compare to expected output
            self.assertTrue(np.sum(np.abs(Ys[i] - Ypred)) < 1E-3, 'expected %s but got %s' % (str(Ys[i]), str(Ypred)))

    def test_lr_to_training_data(self):
        # test the conversion for an example of the bracket language
        rules = [
            ('SS', '', 2, 'S'),
            ('(S)', '', 3, 'S'),
            ('(', ')', 0, 'S')
        ]
        parser = cfg_utils.LRParser(rules, 'S')

        # first, test for the trivial case of an empty input string
        Xs, pops, pushs, shifts, Ys = rsm.lr_to_training_data(parser, [''], ['(', ')'])
        self.assertEqual(1, len(Xs))
        self.assertEqual((0, 2), Xs[0].shape)
        self.assertEqual([[[]]], pops)
        self.assertEqual([[[]]], pushs)
        self.assertEqual(1, len(shifts))
        self.assertEqual(0, len(shifts[0]))
        self.assertEqual([np.array([0])], Ys)

        # set up a few strings
        strings = ['(', '()', '(())', '(())()']

        Xs, pops, pushs, shifts, Ys = rsm.lr_to_training_data(parser, strings)

        # set up expected values
        Xs_exp = [
            np.array([[1., 0.]]),
            np.array([[1., 0.], [0., 1.]]),
            np.array([[1., 0.], [1., 0.], [0., 1.], [0., 1.]]),
            np.array([[1., 0.], [1., 0.], [0., 1.], [0., 1.], [1., 0.], [0., 1.]])
        ]
        pops_exp = [
            [[], []],
            [[], [0], [3]],
            [[], [], [0], [3], [3]],
            [[], [], [0], [3], [3], [0], [3, 2]]
        ]
        pushs_exp = [
            [[], []],
            [[], [1], [1]],
            [[], [], [1], [1], [1]],
            [[], [], [1], [1], [1], [1], [1, 1]]
        ]
        Ys_exp = [
            np.array([0., 0.]),
            np.array([0., 0., 1.]),
            np.array([0., 0., 0., 0., 1.]),
            np.array([0., 0., 0., 0., 1., 0., 1.])
        ]

        # compare to actual values
        for i in range(len(Xs)):

            np.testing.assert_almost_equal(Xs_exp[i], Xs[i], err_msg = 'problem for string %s' % strings[i])

            self.assertEqual(len(pops_exp[i]), len(pops[i]), 'problem for string %s' % strings[i])
            for t in range(len(pops_exp[i])):
                np.testing.assert_almost_equal(pops_exp[i][t], pops[i][t], err_msg = 'problem for string %s' % strings[i])

            self.assertEqual(len(pushs_exp[i]), len(pushs[i]), 'problem for string %s' % strings[i])
            for t in range(len(pushs_exp[i])):
                np.testing.assert_almost_equal(pushs_exp[i][t], pushs[i][t], err_msg = 'problem for string %s' % strings[i])

            np.testing.assert_almost_equal(np.ones(len(strings[i])), shifts[i], err_msg = 'problem for string %s' % strings[i])

            np.testing.assert_almost_equal(Ys_exp[i], Ys[i], err_msg = 'problem for string %s' % strings[i])



        # test the conversion for an example from the JSON language
        parser = json_language.JSON_PARSER

        strings = ['n', '[s]']

        alphabet = ['n', 's', ',', ' ', '{', '}', 'k', ':', '[', ']']

        Xs, pops, pushs, shifts, Ys = rsm.lr_to_training_data(parser, strings, alphabet = alphabet)

        # set up expected values
        Xs_exp = rsm.one_hot_coding(strings, alphabet)

        pops_exp = [
            [[], [1]],
            [[], [], [1, 1], [3]]
        ]
        pushs_exp = [
            [[], [3]],
            [[], [], [3, 1], [3]]
        ]
        Ys_exp = [
            np.array([0., 1.]),
            np.array([0., 0., 0., 1.])
        ]

        # compare to actual values
        for i in range(len(Xs)):

            np.testing.assert_almost_equal(Xs_exp[i], Xs[i], err_msg = 'problem for string %s' % strings[i])

            self.assertEqual(len(pops_exp[i]), len(pops[i]), 'problem for string %s' % strings[i])
            for t in range(len(pops_exp[i])):
                np.testing.assert_almost_equal(pops_exp[i][t], pops[i][t], err_msg = 'problem for string %s' % strings[i])

            self.assertEqual(len(pushs_exp[i]), len(pushs[i]), 'problem for string %s' % strings[i])
            for t in range(len(pushs_exp[i])):
                np.testing.assert_almost_equal(pushs_exp[i][t], pushs[i][t], err_msg = 'problem for string %s' % strings[i])

            np.testing.assert_almost_equal(np.ones(len(strings[i])), shifts[i], err_msg = 'problem for string %s' % strings[i])

            np.testing.assert_almost_equal(Ys_exp[i], Ys[i], err_msg = 'problem for string %s' % strings[i])



    def test_rsm_training(self):
        # we try to learn the bracket grammar. First, set up a LR parser for it
        rules = [
            ('SS', '', 2, 'S'),
            ('(S)', '', 3, 'S'),
            ('(', ')', 0, 'S')
        ]
        parser = cfg_utils.LRParser(rules, 'S')

        # set up some training strings
        strings = ['()', '(())', '(())()()', '(()())']

        # convert it to training data for the RSM
        Xs, pops, pushs, shifts, Ys = rsm.lr_to_training_data(parser, strings)

        # set up a manually engineered reservoir for now, which should work
        U = np.zeros((6, 2))
        U[0, 0] = 1.
        U[1, 1] = 1.
        # set up coding matrix for nonterminal symbols
        V = np.zeros((6, 1))
        V[2, 0] = 1.
        # set up reservoir matrix
        W = np.zeros((6, 6))
        W[3, 0] = 1.
        W[4, 1] = 1.
        W[5, 2] = 1.
        W = csr_matrix(W)

        # set up RSM model
        model = rsm.RSM(U, V, W, classify_output = True)

        # fit it to the data
        model.fit(Xs, pops, pushs, shifts, Ys)

        # check its performance on the training data
        for i in range(len(Xs)):
            Ypred = model.predict(Xs[i])
            np.testing.assert_almost_equal(Ys[i], Ypred, err_msg = 'problem for string %s' % strings[i])

        # test generalization on a longer sequence
        word = '()(())((())())'
        X = np.zeros((len(word), 2))
        for t in range(len(word)):
            if word[t] == '(':
                X[t, 0] = 1.
            else:
                X[t, 1] = 1.
        _, Y = parser.parse(word)

        Ypred = model.predict(X)

        np.testing.assert_almost_equal(Y, Ypred, err_msg = 'problem for string %s' % word)


    def test_rsm_training2(self):
        # try the same training as in the other test, but this time with
        # a general purpse reservoir, in particular a LMU

        # set up a LR parser for the bracket grammar
        rules = [
            ('SS', '', 2, 'S'),
            ('(S)', '', 3, 'S'),
            ('(', ')', 0, 'S')
        ]
        parser = cfg_utils.LRParser(rules, 'S')

        # set up some training strings
        strings = ['()', '(())', '(())()()', '(()())']

        # convert it to training data for the RSM
        Xs, pops, pushs, shifts, Ys = rsm.lr_to_training_data(parser, strings)

        # set up a reservoir
        U, W = lmu.initialize_reservoir(3, 1, 2)
        V = np.expand_dims(U[:, 2], 1)
        U = U[:, :2]

        # set up RSM model
        model = rsm.RSM(U, V, W, classify_output = True)

        # fit it to the data
        model.fit(Xs, pops, pushs, shifts, Ys)

        # check its performance on the training data
        for i in range(len(Xs)):
            Ypred = model.predict(Xs[i])
            np.testing.assert_almost_equal(Ys[i], Ypred, err_msg = 'problem for string %s' % strings[i])

        # test generalization on a longer sequence
        word = '()(())((())())'
        X = np.zeros((len(word), 2))
        for t in range(len(word)):
            if word[t] == '(':
                X[t, 0] = 1.
            else:
                X[t, 1] = 1.
        _, Y = parser.parse(word)

        Ypred = model.predict(X)

        np.testing.assert_almost_equal(Y, Ypred, err_msg = 'problem for string %s' % word)



        # test an example with the JSON language
        parser   = json_language.JSON_PARSER
        alphabet = ['n', 's', ',', ' ', '{', '}', 'k', ':', '[', ']']
        strings  = ['n', '[s]', '[n, [n, s], s]', '{k : [s, []], k : {k : n}}']

        # convert to training data for the RSM
        Xs, pops, pushs, shifts, Ys = rsm.lr_to_training_data(parser, strings, alphabet = alphabet)

        # set up a reservoir
        U, W = lmu.initialize_reservoir(len(alphabet) + 3, 7, 8)

        V = U[:, len(alphabet):]
        U = U[:, :len(alphabet)]

        # set up RSM model
        model = rsm.RSM(U, V, W, classify_output = True)

        # fit it to the data
        model.fit(Xs, pops, pushs, shifts, Ys)

        # check its performance on the training data
        for i in range(len(Xs)):
            Ypred = model.predict(Xs[i])
            np.testing.assert_almost_equal(Ys[i], Ypred, err_msg = 'problem for string %s' % strings[i])

        # check generalization on a longer word
        word = '{k : s, k : s, k : [n, [], n], k : {k : n, k : []}}'

        X    = rsm.one_hot_coding([word], alphabet)[0]
        _, Y = parser.parse(word)

        Ypred = model.predict(X, max_inner_loop = 5)

        np.testing.assert_almost_equal(Y, Ypred, err_msg = 'problem for string %s' % word)


    def test_rsm_shift_training(self):
        # test a case where we do not try to learn the behavior from a LR
        # parser. Instead, we try to solve the repeat copy task with fixed
        # input length. For that, we need to shift the input onto the stack
        # and then recall the stack content whenever an end-of-sequence token
        # is in the input

        # length of the input
        T = 4
        # number of dimensions
        n = 8
        # number of training data points
        N = 20

        def sample_input():
            # sample the number of repeats randomly
            R = np.random.randint(3, 10)
            # set the sequence length
            Tau = (T+1)*(R+1)-1
            # initialize an input and output sequence
            X = np.zeros((Tau, n+1))
            Y = np.zeros((Tau+1, n))
            # fill the input randomly
            X[:T, :n] = np.random.randint(2, size = (T, n))
            # copy this input to the output for each repeat,
            # preceded by a special end-of-sequence token on
            # the input
            for r in range(R):
                lo = T + r*(T+1)
                hi = T + (r+1)*(T+1)
                X[lo, -1] = 1.
                Y[lo+1:hi, :n] = X[:T, :n]

            return X, Y

        # generate some training data
        Xs     = []
        pops   = []
        pushs  = []
        shifts = []
        Ys     = []
        for i in range(N):
            X, Y = sample_input()
            Xs.append(X)
            pops.append([[]] * Y.shape[0])
            pushs.append([[]] * Y.shape[0])
            shift = np.zeros(X.shape[0])
            shift[:T] = 1.
            shifts.append(shift)
            Ys.append(Y)

        # set up a reservoir
        U, W = lmu.initialize_reservoir(n+1, T, T)
        V = np.zeros((U.shape[0], 0))

        # set up RSM model
        model = rsm.RSM(U, V, W, C = 10000., classify_output = True)

        # fit it to the data
        model.fit(Xs, pops, pushs, shifts, Ys)

        # check the output
        errs = np.zeros(N)
        for i in range(N):
            Ypred = model.predict(Xs[i])
            # count the MAE
            errs[i] = np.mean(np.abs(Ypred - Ys[i]))
        # ensure that the error is close to zero
        self.assertTrue(np.mean(errs) < 0.01)

if __name__ == '__main__':
    unittest.main()
