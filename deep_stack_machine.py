"""
A deep version of the reservoir stack machine, implemented in pytorch.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import torch

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

class DeepStackMachine(torch.nn.Module):
    """ A reservoir stack machine which can losslessly store inputs and
    discrete symbols over time.

    The reservoir stack machine emulates a LR(1) parser for context free
    languages. In particular, it consists of two recurrent networks, and a
    stack that is controlled by three classifiers: a pop classifier which
    controls how many elements we pop from the stack, a push classifier which
    controls whether and which nonterminal symbol we push onto the stack, and
    a shift classifier which decides whether to push the current input symbol
    into the stack. The input to these classifiers is the concatenation of the
    recurrent memory state for the input and the stack.

    The stack is adjusted according to each pop, push, and shift decision.
    The classifiers for pop, push, and shift are implemented as linear layers.

    Attributes
    ----------
    dim_in: int
        The input dimensionality.
    dim_hid: int
        The number of state neurons.
    dim_out: int
        The number of outputs.
    num_nonts: int
        The number of nonterminal symbols k.
    max_pop: int
        The maximum number of popped symbols in one step.
    classify_output: bool (default = False)
        If set to True, a classifier is used to construct the output instead
        of a regression layer. In this case, dim_out is treated as the number
        of classes.
    in_rnn_: class torch.nn.GRU
        The input GRU with dim_in inputs and dim_hid neurons.
    stack_rnn_: class torch.nn.GRU
        The stack GRU with dim_in + num_nonts inputs and dim_hid neurons.
    c_pop_: class torch.nn.Linear
        The pop classification layer.
    c_push_: class torch.nn.Linear
        The push classification layer.
    c_shift_: class torch.nn.Linear
        The shift classification layer.
    c_out_: class torch.nn.Linear
        The output layer.

    """
    def __init__(self, dim_in, dim_hid, dim_out, num_nonts, max_pop, classify_output = False):
        super(DeepStackMachine, self).__init__()
        self.dim_in    = dim_in
        self.dim_hid   = dim_hid
        self.dim_out   = dim_out
        self.num_nonts = num_nonts
        self.max_pop   = max_pop
        self.classify_output = classify_output

        # initialize neural network layers
        self.in_rnn_    = torch.nn.GRU(self.dim_in, self.dim_hid)
        self.stack_rnn_ = torch.nn.GRU(self.dim_in + self.num_nonts, self.dim_hid)
        self.c_pop_     = torch.nn.Linear(2 * self.dim_hid, self.max_pop + 1)
        self.c_push_    = torch.nn.Linear(2 * self.dim_hid, self.num_nonts + 1)
        self.c_shift_   = torch.nn.Linear(2 * self.dim_hid, 1)
        self.c_out_     = torch.nn.Linear(2 * self.dim_hid, self.dim_out)

    def compute_loss(self, X, pop, push, shift, Y, verbose = False):
        """ Computes the loss of the current network on the given data
        using teacher forcing for pop, push, and shift actions.

        Parameters
        ----------
        X: class torch.Tensor of size T x self.dim_in
            The input time series with T time steps.
        pop: list of lists
            The time series of desired pop actions. Note that multiple pop
            actions are permitted per time step, so pop[t] should contain all
            desired pop actions for step t. In particular, pop[t][j] should be
            the number of elements to be popped from the stack.
        push: list of lists
            The time series of desired push actions. Note that multiple push
            actions are permitted per time step, so push[t] should contain all
            desired push actions for step t. In particular, push[t][j] should
            be the index of the nonterminal symbol to be pushed onto the stack.
            If push[t][j] is zero, no nonterminal symbol will be pushed. Note
            that len(pop[t]) must be equal to len(push[t]).
        shift: list of length T
            The time series of desired shift actions
        Y: torch.Tensor of length T x self.dim_out
            The time series of desired outputs.

        Returns
        -------
        loss: class torch.nn.Tensor
            The current loss.

        """
        T = X.shape[0]

        # verify length consistency
        if len(pop) != T+1:
            raise ValueError('Problem with the %dth time series: Expected %d time steps in pop time series but was %d versus %d.' % (i, T+1, len(pop)))
        if len(push) != T+1:
            raise ValueError('Problem with the %dth time series: Expected %d time steps in push time series but was %d versus %d.' % (i, T+1, len(push)))
        if len(shift) != T:
            raise ValueError('Problem with the %dth time series: Expected %d time steps in shift time series but was %d versus %d.' % (i, T, len(shift)))
        if len(Y) != T+1:
            raise ValueError('Problem with the %dth time series: Expected %d time steps in output time series but was %d versus %d.' % (i, T+1, len(Y)))

        # initialize the stack
        stk = []

        # compute states based on input sequence
        H, _ = self.in_rnn_(torch.cat((X, torch.zeros(1, self.dim_in)), 0).unsqueeze(1))
        H = H[:, 0, :]

        if self.classify_output and len(Y.shape) == 1:
            Y = Y.unsqueeze(1)

        # initialize stack state as zero
        g = torch.zeros(self.dim_hid)
        # initialize loss
        loss = torch.zeros(1)
        # iterate over time series
        for t in range(X.shape[0]):
            if verbose:
                print('--- time step %d ---' % t)
            # compute iteration
            loss_t, g = self.train_iteration_(X[t, :], H[t, :], g, stk, pop[t], push[t], shift[t], Y[t, :], verbose)
            loss = loss + loss_t

        if verbose:
            print('--- end of sequence ---')
        # do final iteration
        loss_end, _ = self.train_iteration_(None, H[-1, :], g, stk, pop[-1], push[-1], None, Y[-1, :], verbose)

        loss = loss + loss_end

        return loss

    def train_iteration_(self, x, h, g, stk, pop, push, shift, y, verbose = False):
        """ Computes the loss for the given input vector, states
        and stack, as well as the given pop, push, and shift commands.

        Returns
        -------
        loss: class torch.Tensor
            The loss in the current step.
        g: class torch.Tensor
            The next stack state.

        """
        # concatenate input and stack state
        hg = torch.cat((h, g))

        # verify that the length of pop and push agrees
        if len(pop) != len(push):
            raise ValueError('There were %d pop actions but %d push actions. These numbers need to be the same.' % (t, i, len(pop), len(push)))

        # initialize tensors for pop and push predictions
        pop_pred  = torch.zeros(len(pop)+1, self.max_pop + 1)
        push_pred = torch.zeros(len(pop)+1, self.num_nonts + 1)

        for j in range(len(pop)):

            # record predicted pop and push actions
            pop_pred[j, :]  = self.c_pop_(hg)
            push_pred[j, :] = self.c_push_(hg)

            if verbose:
                print('predicted pop = %s and was %d' % (str(pop_pred[j, :].detach().numpy()), pop[j]))

            # if the training data imposes a pop action, apply it
            if pop[j] > 0:
                # apply the pop action
                for r in range(pop[j]):
                    stk.pop()
                if len(stk) == 0:
                    # if the stack is empty now, replace the stack state with zero
                    g = torch.zeros(self.dim_hid)
                else:
                    # otherwise, encode the stack after popping symbols
                    stk_encoding = torch.zeros(len(stk), self.dim_in + self.num_nonts)
                    for i in range(len(stk)):
                        if isinstance(stk[i], int):
                            # encode a nonterminal via one-hot coding
                            stk_encoding[i, self.dim_in + stk[i] - 1] = 1.
                        else:
                            stk_encoding[i, :self.dim_in] = stk[i].detach()
                    # and update g
                    _, g = self.stack_rnn_(stk_encoding.unsqueeze(1))
                    g = g[0, 0, :]

            if verbose:
                print('predicted push = %s and was %d' % (str(push_pred[j, :].detach().numpy()), push[j]))

            # apply the push action if so desired
            if push[j] > 0:
                # push nonterminal onto the stack
                stk.append(push[j])
                # update stack state
                stk_encoding = torch.zeros(1, 1, self.dim_in + self.num_nonts)
                stk_encoding[0, 0, self.dim_in + push[j] - 1] = 1.
                _, g = self.stack_rnn_(stk_encoding, g.unsqueeze(0).unsqueeze(1))
                g = g[0, 0, :]
            # update hg
            hg = torch.cat((h, g))

            if verbose:
                print('current stack: %s' % str(stk))

        # end each pop and push phase with a zero action
        pop_pred[-1, :]  = self.c_pop_(hg)
        push_pred[-1, :] = self.c_push_(hg)
        pop = pop + [0]
        push = push + [0]

        if x is not None:

            # compute predicted shift action
            shift_pred = self.c_shift_(hg)

            # apply the shift action if desired
            if shift > 0.5:
                if verbose:
                    print('predicted shift = %g and was %g' % (shift_pred.item(), shift))
                # push input symbol onto the stack
                stk.append(x)
                # update stack state
                stk_encoding = torch.zeros(1, 1, self.dim_in + self.num_nonts)
                stk_encoding[0, 0, :self.dim_in] = x.detach()
                _, g = self.stack_rnn_(stk_encoding, g.unsqueeze(0).unsqueeze(1))
                g = g[0, 0, :]

                if verbose:
                    print('stack after shift: %s' % str(stk))

        # compute predicted output
        y_pred = self.c_out_(hg)

        if verbose:
            print('predicted output %s and was %s' % (str(y_pred.detach().numpy()), str(y.detach().numpy())))

        # compute loss as sum of pop classifiction and push classification
        if len(pop) > 0:
            pop_loss  = torch.nn.functional.cross_entropy(pop_pred, torch.tensor(pop, dtype=torch.long))
            push_loss = torch.nn.functional.cross_entropy(push_pred, torch.tensor(push, dtype=torch.long))
            if verbose:
                print('pop loss = %g' % pop_loss.item())
                print('push loss = %g' % push_loss.item())
        else:
            pop_loss = 0.
            push_loss = 0.

        # add shift classification
        if x is not None:
            shift_loss = torch.nn.functional.binary_cross_entropy_with_logits(shift_pred, torch.tensor(shift).unsqueeze(0))
            if verbose:
                print('shift loss = %g' % shift_loss.item())
        else:
            shift_loss = 0.

        # add output loss
        if self.classify_output:
            out_loss = torch.nn.functional.cross_entropy(y_pred.unsqueeze(0), y)
        else:
            out_loss = torch.nn.functional.mse_loss(y_pred.unsqueeze(0), y.unsqueeze(0))

        if verbose:
            print('output loss = %g' % out_loss.item())

        loss = out_loss + pop_loss + push_loss + shift_loss

        return loss, g

    def forward(self, X, max_inner_loop = None, verbose = False):
        """ Returns the predicted output sequence fo the given input sequence.

        Parameters
        ----------
        X: class torch.tensor of size T x self.dim_in
            The input time series.
        max_inner_loop: int (default = None)
            To prevent endless loops, we can, as a last resort, stop after a
            fixed number of iterations.
        verbose: bool (default = False)
            If set to True, this function will print verbose messages regarding
            its internal state during computation.

        Returns
        -------
        Y: class torch.tensor of size T + 1 x self.dim_out
            The output time series with T+1 time steps where the last output
            is produced after the last input symbol has been read.

        """
        T = X.shape[0]

        # initialize the stack
        stk = []

        # compute states based on input sequence
        H, _ = self.in_rnn_(torch.cat((X, torch.zeros(1, self.dim_in)), 0).unsqueeze(1))
        H = H[:, 0, :]

        # initialize the stack state
        g = torch.zeros(self.dim_hid)

        # start processing
        Y = []
        for t in range(T):
            if verbose:
                print('--- time step %d ---' % t)

            # do a prediction iteration
            y, g = self.forward_iteration_(X[t, :], H[t, :], g, stk, max_inner_loop, verbose)
            Y.append(y)

        if verbose:
            print('--- end of sequence ---')
        # do a final iteration at the end of the sequence
        y, g = self.forward_iteration_(None, H[-1, :], g, stk, max_inner_loop, verbose)
        Y.append(y)

        Y = torch.stack(Y, axis = 0)
        return Y

    def forward_iteration_(self, x, h, g, stk, max_inner_loop, verbose):
        """ Computes a forward iteration for the given input, state, stack
        state and stack.

        Returns
        -------
        y: class torch.Tensor
            The predicted output for the current iteration.
        g: class torch.Tensor
            The next stack state.

        """
        # concatenate input and stack state
        hg = torch.cat((h, g))

        # initialize loop variables
        pop = 1.
        push = 1.

        last_pop = None
        last_push = None

        num_loops = 0

        # start a loop until neither pop nor push options occur
        while True:
            num_loops += 1
            if max_inner_loop is not None and num_loops > max_inner_loop:
                if verbose:
                    print('Had to break an endless loop')
                break

            # predict pop and push action
            pop  = min(len(stk), torch.argmax(self.c_pop_(hg)).item())
            push = torch.argmax(self.c_push_(hg)).item()

            if pop == 0 and push == 0:
                break

            if last_push is not None and last_pop == 0 and last_push == push:
                # prevent endless loops
                if verbose:
                    print('Had to break an endless loop')
                break

            if verbose:
                print('pop prediction: %d' % pop)

            if pop > 0:
                # apply the pop action
                for r in range(pop):
                    stk.pop()
                if len(stk) == 0:
                    # if the stack is empty now, replace the stack state with zero
                    g = torch.zeros(self.dim_hid)
                else:
                    # otherwise, encode the stack after popping symbols
                    stk_encoding = torch.zeros(len(stk), self.dim_in + self.num_nonts)
                    for i in range(len(stk)):
                        if isinstance(stk[i], int):
                            # encode a nonterminal via one-hot coding
                            stk_encoding[i, self.dim_in + stk[i] - 1] = 1.
                        else:
                            stk_encoding[i, :self.dim_in] = stk[i].detach()
                    # and update g
                    _, g = self.stack_rnn_(stk_encoding.unsqueeze(1))
                    g = g[0, 0, :]

            if verbose:
                print('push prediction: %d' % push)

            # apply the push action if so desired
            if push > 0:
                # push nonterminal onto the stack
                stk.append(push)
                # update stack state
                stk_encoding = torch.zeros(1, 1, self.dim_in + self.num_nonts)
                stk_encoding[0, 0, self.dim_in + push - 1] = 1.
                _, g = self.stack_rnn_(stk_encoding, g.unsqueeze(0).unsqueeze(1))
                g = g[0, 0, :]

            last_push = push
            last_pop  = pop

            # update hg
            hg = torch.cat((h, g))

            if verbose:
                print('current stack: %s' % str(stk))
                # TODO REMOVE
                print(hg.detach().numpy())

        if x is not None:

            # compute predicted shift action
            shift = self.c_shift_(hg)

            if verbose:
                print('shift prediction: %g' % shift.item())

            # apply the shift action if desied
            if shift > 0.:
                # push input symbol onto the stack
                stk.append(x)

                if verbose:
                    print('stack after shift: %s' % str(stk))

                # update stack state
                stk_encoding = torch.zeros(1, 1, self.dim_in + self.num_nonts)
                stk_encoding[0, 0, :self.dim_in] = x.detach()
                _, g = self.stack_rnn_(stk_encoding, g.unsqueeze(0).unsqueeze(1))
                g = g[0, 0, :]

        # compute predicted output
        y = self.c_out_(hg)

        if verbose:
            print('out prediction: %s' % str(y.detach().numpy()))

        if self.classify_output:
            y = torch.argmax(y)

        return y, g
