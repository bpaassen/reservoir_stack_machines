#!/usr/bin/python3
"""
Tests the GRU interface.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import torch
import numpy as np
import dyck_languages
import cfg_utils
import rsm
import simple_gru

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'


class TestSimpleGRU(unittest.TestCase):

    def test_training(self):
        # we try to learn the bracket grammar. First, set up a LR parser for it
        parser = dyck_languages.D1_PARSER

        # set up some training strings
        strings = ['()', '(())', '(())()()', '(()())']

        # construct one-hot-coding of the input and output representation
        Xs = rsm.one_hot_coding(strings)
        Ys = []
        for i in range(len(strings)):
            Ys.append(parser.parse(strings[i])[1])

        # initialize a simple gru
        model = simple_gru.SimpleGRU(dim_in = 2, dim_hid = 8, dim_out = 2, classify_output = True)

        # initialize an optimizer
        optimizer = torch.optim.Adam(model.parameters(), lr = 2E-3, weight_decay = 1E-8)

        loss_avg = None

        for epoch in range(1000):
            optimizer.zero_grad()
            # sample an input string
            i = np.random.choice(len(Xs))
            # compute the loss
            loss = model.compute_loss(torch.tensor(Xs[i], dtype=torch.float), torch.tensor(Ys[i], dtype=torch.long))

            # compute gradient
            loss.backward()

            # perform optimization step
            optimizer.step()

            if loss_avg is None:
                loss_avg = loss.item()
            else:
                loss_avg = 0.9 * loss_avg + 0.1 * loss.item()

            if (epoch + 1) % 100 == 0:
                print('loss avg after %d epochs: %g' % (epoch + 1, loss_avg))

        # check its performance on the training data
        for i in range(len(Xs)):
            Ypred = model.forward(torch.tensor(Xs[i], dtype=torch.float)).detach().numpy()
            np.testing.assert_allclose(Ys[i], Ypred, atol = 0.1, err_msg = 'problem for string %s' % strings[i])


if __name__ == '__main__':
    unittest.main()
