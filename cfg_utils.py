"""
Implements utility functions to handle LR(1) parseable context free grammars.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

_NONT_STARTING_ = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

class CFGGrammar:
    """ Implements a context-free grammar given the context-free rules and
    the starting symbol.

    Attributes
    ----------
    rules: dict
        A dictionary mapping a left-hand-side nonterminal symbol to a list
        of possible right-hand-sides. Note that we assume that nonterminals
        start with A-Z (uppercase single character) and terminals do not.
        If symbols consist of more than one character, the right hand side
        needs to be a list instead of a string.
    start: string
        The starting nonterminal symbol.

    """
    def __init__(self, rules, start):
        self.rules = rules
        self.start = start

    def sample_word(self, ps = None):
        """ Randomly samples a word from a stochastic version of this grammar.

        Per default, uniform probabilities over all rules are assumed. If ps
        is given, these probabilities are used instead.

        Parameters
        ----------
        ps: dict (default = None)
            A dictionary mapping left-hand-side nonterminals to probability
            distributions over grammar rules. If an entry is not given, a
            uniform distribution is assumed.

        Returns
        -------
        w: string
            A word from this context-free language

        """
        if ps is None:
            ps = {}

        # start recursively sampling a word
        w = []
        self.sample_word_(w, self.start, ps)

        return ''.join(w)

    def sample_word_(self, w, nont, ps):
        """ Internal subroutine to sample a word from the language.

        Parameters
        ----------
        w: list
            A list for the characters in the finished word.
        nont: string
            The current nonterminal symbol.
        ps: dict
            A dictionary mapping left-hand-side nonterminals to probability
            distributions over grammar rules. If an entry is not given, a
            uniform distribution is assumed.

        """
        # retrieve the rules for the current nonterminal
        rules = self.rules[nont]
        # retrieve the rule probabilities for the current nonterminal
        if nont not in ps:
            p = None
        else:
            p = ps[nont]

        # select a rule randomly
        r = np.random.choice(len(self.rules[nont]), p = p)
        rule = rules[r]

        # apply the rule
        for symbol in rule:
            if symbol[0] in _NONT_STARTING_:
                # if the symbol is a nonterminal symbol, apply recursion
                self.sample_word_(w, symbol, ps)
            else:
                # otherwise append to w
                w.append(symbol)

class LRParser:
    """ Implements an LR(1) parser.

    Attributes
    ----------
    rules: list
        A list of 4-tuples (s, x, K, A) of a stack suffix s, an input symbol x,
        a number of symbols to pop from the stack K, and a nonterminal to push
        to the stack A. During parsing, the first matching rule is always
        applied. If no rule matches, a shift rule is applied.
    start: string
        The starting symbol. The parser accepts an input word if, after parsing
        the word completely, only the starting symbol is left on the stack.

    """
    def __init__(self, rules, start):
        self.rules = rules
        self.start = start

    def parse(self, w, verbose = False):
        """ Attempts to parse the given word w with this LRParser.

        Parameters
        ----------
        w: string
            The input word to be parsed.
        verbose: bool (default = False)
            If set to True, this function prints detailed progress information about the parse.

        Returns
        -------
        rules: list of length len(w)+1
            A list where rules[t] is a list of reduce rules applied at each
            time step.
        y: list of length len(w)+1
            A list where y[t] is 1 iff the prefix w[:t] is a valid word
            according to the grammar and is 0 otherwise.

        """
        # initialize the stack
        stk = []
        # initialize the output
        rules = []
        y = []
        # begin parsing the input word
        for x in w:
            self.iteration_(stk, rules, y, x, verbose)
        # do another iteration for the end of the word
        self.iteration_(stk, rules, y, '', verbose)
        return rules, y

    def iteration_(self, stk, rules, y, x, verbose = False):
        if verbose:
            if x == '':
                print('end of word')
            else:
                print('next symbol: %s' % x)
        # initialize rule list for current iteration
        rules_iter = []
        # as long as we find a matching rule, work on the stack
        while True:
            found_rule = False
            for rule in self.rules:
                # check if the input symbol matches
                if len(rule[1]) > 0 and rule[1] != x:
                    continue
                # check if the stack suffix matches
                s = rule[0]
                if s == '#':
                    # if the suffix is '#', this denotes the empty stack
                    if len(stk) > 0:
                        continue
                else:
                    # otherwise we check whether the stack is long enough
                    # for the suffix
                    if len(stk) < len(s):
                        continue
                    # and we check the stack content
                    if len(s) > 0:
                        if s != ''.join(stk[-len(s):]):
                            continue
                # if so, break off the search
                found_rule = True
                break
            if not found_rule:
                break
            if verbose:
                print('found matching rule: %s' % str(rule))
            rules_iter.append(rule)
            # apply the rule
            K = rule[2]
            A = rule[3]
            if K > len(stk):
                raise ValueError('Parsing error: The rule %s matched but the stack only had %d elements' % (str(rule), len(stk)))
            # pop K elements
            for k in range(K):
                stk.pop()
            # push A on the stack
            stk.append(A)
            no_rules = False
            if verbose:
                print('stack state: %s' % str(stk))
        rules.append(rules_iter)
        # check whether the current prefix is a valid word
        if len(stk) == 1 and stk[0] == self.start:
            y.append(1)
            if verbose:
                print('accept')
        else:
            y.append(0)
            if verbose:
                print('decline')
        # apply a shift
        stk.append(x)
        if verbose and x != '':
            print('stack state: %s' % str(stk))
