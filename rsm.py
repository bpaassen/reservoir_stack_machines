"""
Implements reservoir stack machines.

"""

#from sklearn.decomposition import PCA
#import matplotlib.pyplot as plt

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin, ClassifierMixin
from sklearn.svm import SVC
from sklearn.linear_model import Ridge

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

class ConstantClassifier(BaseEstimator, ClassifierMixin):
    """ A classifier that always returns the same label.

    Attributes
    ----------
    y: float
        The label to assign.

    """
    def __init__(self, y):
        self.y = y

    def fit(self, X, Y):
        raise ValueError('Not supported.')

    def predict(self, X):
        return np.full(X.shape[0], self.y)

class RSM(BaseEstimator, RegressorMixin):
    """ A reservoir stack machine which can losslessly store inputs and
    discrete symbols over time.

    The reservoir stack machine emulates a LR(1) parser for context free
    languages. In particular, it consists of an echo state network and a stack
    that is controlled by three classifiers, a pop classifier which controls
    how many elements we pop from the stack, a push classifier which controls
    whether and which nonterminal symbol we push onto the stack, and a shift
    classifier which decides whether to push the current input symbol into the
    stack.

    The regular state dynamics are like that of a normal echo state network:

    h[t] = self.nonlin(self.U * x[t] + self.W * h[t-1])

    However, we concatenate this state with another reservoir state that
    describes the current stack content. In particular, let s[0] ... s[R]
    be the current stack. Then, the reservoir state is computed as g[R]
    with the recurrence g[0] = 0,

    g[r] = self.nonlin(self.U * s[r] + self.W * g[r-1])

    if s[r] is an input symbol and

    g[r] = self.nonlin(self.V * s[r] + self.W * g[r-1])

    if s[r] is a nonterminal symbol.

    The output y[t] is then computed linearly via

    y[t] = self.Wout * (h[t], g[t])

    The stack is adjusted according to each pop, push, and shift decision.
    The classifiers for pop, push, and shift are implemented as SVMs.

    Attributes
    ----------
    U: ndarray of size m x n
        The input-to-reservoir connection matrix.
    V: ndarray of size m x k
        The nonterminal-to-reservoir connection matrix.
    W: ndarray of size m x m
        The reservoir matrix; this should be a scipy.sparse matrix.
    nonlin: function (default = identity function)
        The nonlinearity to be applied when computing the reservoir state.
    C: float (default = 100.)
        The regularization parameter for the SVM classifiers.
    gamma: float or string (default = 'auto')
        The gamma parameter for the SVM classifiers. If set to None, a linear
        SVM is used instead of a RBF-SVM.
    classify_output: bool (default = False)
        If set to True, a classifier is used to construct the output instead of
        a linear regression.
    regul: float (default = 1E-5)
        The regularization parameter for the output linear regression.
    c_pop_: class sklearn.svm.SVC
        The pop action classifier (after training).
    c_push_: class sklearn.svm.SVC
        The pop action classifier (after training).
    c_shift_: class sklearn.svm.SVC
        The pop action classifier (after training).
    c_out_: class sklearn.svm.SVC OR class sklearn.linear_model.Ridge
        The output classifier/regressor (after training).

    """
    def __init__(self, U, V, W, nonlin = lambda x : x, C = 100., gamma = 'auto', classify_output = False, regul = 1E-5):

        self.U = U
        self.V = V
        self.W = W
        self.nonlin = nonlin
        self.C = C
        self.gamma = gamma
        self.classify_output = classify_output
        self.regul = regul

    def fit_classifier_(self, name, H, Y, verbose = False):
        """ Fits a classifier to the given data. """
        if verbose:
            print('start training classfier %s' % name)

        unique_labels = np.unique(Y)
        if len(unique_labels) == 1:
            model = ConstantClassifier(unique_labels[0])

            if verbose:
                print('Found only single possible label (%s); using a constant classifier.' % str(unique_labels[0]))
        else:
            if self.gamma is None:
                model = SVC(C = self.C, kernel = 'linear')
            else:
                model = SVC(C = self.C, kernel = 'rbf', gamma = self.gamma)
            model.fit(H, Y)

            if verbose:
                print('trained %s classifier with accuracy %g' % (name, model.score(H, Y)))

        return model


    def fit(self, X, pop, push, shift, Y, verbose = False):
        """ Fits this reservoir stack machine to the given data.

        Parameters
        ----------
        X: ndarray of size T x n
            The input time series with T time steps and n features where n
            must be equal to self.U.shape[1]
        pop: list of lists
            The time series of desired pop actions. Note that multiple pop
            actions are permitted per time step, so pop[t] should contain all
            desired pop actions for step t. In particular, pop[t][j] should be
            the number of elements to be popped from the stack.
        push: list of lists
            The time series of desired push actions. Note that multiple push
            actions are permitted per time step, so push[t] should contain all
            desired push actions for step t. In particular, push[t][j] should
            be the index of the nonterminal symbol to be pushed onto the stack.
            If push[t][j] is zero, no nonterminal symbol will be pushed. Note
            that len(pop[t]) must be equal to len(push[t]).
        shift: ndarray of length T
            The time series of desired shift actions
        Y: ndarray of length T x L
            The time series of desired outputs.
        verbose: bool (default = False)
            If set to True, this function will print verbose messages regarding
            its internal state during computation.


        Returns
        -------
        self

        """

        # check reservoir
        m, n = self.U.shape

        if self.W.shape[0] != m or self.W.shape[1] != m:
            raise ValueError('Expected a %d x %d reservoir matrix W but had a %d x %d matrix.' % (m, m, self.W.shape[0], self.W.shape[1]))


        # prepare the input list representation
        if isinstance(X, list):
            Xs     = X
            pops   = pop
            pushs  = push
            shifts = shift
            Ys     = Y
            if len(Xs) != len(pops):
                raise ValueError('Expected as many pop time series as input time series, but got %d input time series and %d action time series' % (len(Xs), len(pops)))
            if len(Xs) != len(pushs):
                raise ValueError('Expected as many push time series as input time series, but got %d input time series and %d pop time series' % (len(Xs), len(pushs)))
            if len(Xs) != len(shifts):
                raise ValueError('Expected as many shift time series as input time series, but got %d input time series and %d pop time series' % (len(Xs), len(shifts)))
            if len(Xs) != len(Ys):
                raise ValueError('Expected as many output as input time series, but got %d input time series and %d output time series' % (len(Xs), len(Ys)))
        else:
            Xs     = [X]
            pops   = [pop]
            pushs  = [push]
            shifts = [shift]
            Ys     = [Y]

        # initialize training data lists for all classifiers
        Hstack = []
        Hshift = []
        Hout   = []

        Ypop   = []
        Ypush  = []
        Yshift = []
        Yout   = []


        for i in range(len(Xs)):

            # retrieve the ith time series
            X     = Xs[i]
            T     = X.shape[0]
            pop   = pops[i]
            push  = pushs[i]
            shift = shifts[i]
            Y     = Ys[i]

            # verify length consistency
            if len(pop) != T+1:
                raise ValueError('Problem with the %dth time series: Expected %d time steps in pop time series but was given %d.' % (i, T+1, len(pop)))
            if len(push) != T+1:
                raise ValueError('Problem with the %dth time series: Expected %d time steps in push time series but was given %d.' % (i, T+1, len(push)))
            if len(shift) != T:
                raise ValueError('Problem with the %dth time series: Expected %d time steps in shift time series but was given %d.' % (i, T, len(shift)))
            if len(Y) != T+1:
                raise ValueError('Problem with the %dth time series: Expected %d time steps in output time series but was given %d.' % (i, T+1, len(Y)))

            # initialize the stack
            stk = []
            # initialize the state
            h = np.zeros(m)
            # initialize the stack state
            g = np.zeros(m)

            for t in range(T):
                # update the state and append state training data
                h, g = self.train_iteration_(X[t, :], h, g, stk, pop[t], push[t], shift[t], Hstack, Hshift, Hout)
                #print(stk, pop[t], shift[t])
                #sys.stdout.flush()
                # append targets for pop and push classifier
                Ypop += pop[t]
                Ypop.append(0)
                #print(Ypop)
                Ypush += push[t]
                Ypush.append(0)

            # apply a final iteration at the end of the sequence
            self.train_iteration_(None, h, g, stk, pop[T], push[T], None, Hstack, Hshift, Hout)

            # append targets for pop and push classifier
            Ypop += pop[T]
            Ypop.append(0)
            Ypush += push[T]
            Ypush.append(0)

            # append targets for shift and output
            Yshift.append(shift)
            Yout.append(Y)

        # concatenate training data
        Hstack = np.concatenate(Hstack, 0)
        Hshift = np.concatenate(Hshift, 0)
        Hout   = np.concatenate(Hout, 0)
        Ypop   = np.array(Ypop)
        Ypush  = np.array(Ypush)
        Yshift = np.concatenate(Yshift, 0)
        Yout   = np.concatenate(Ys, 0)

        if verbose:
            print('recorded %d training data points for pop and push classifier and %d points for shift and output' % (Hstack.shape[0], Hout.shape[0]))

        
        
        #proj  = PCA(n_components=2).fit_transform(Hstack)
        #fig = plt.figure()
        #ax1  = fig.add_subplot(211) # , projection='3d'
        #plt.plot(proj[Ypop==0,0], proj[Ypop==0,1], 'ro')
        #ax2  = fig.add_subplot(212, sharex = ax1, sharey = ax1)
        #plt.plot(proj[Ypop==1,0], proj[Ypop==1,1], 'bo')
        #plt.show()
        
        
        # fit pop classifier
        self.c_pop_  = self.fit_classifier_('pop', Hstack, Ypop, verbose)
        # fit push classifier
        self.c_push_ = self.fit_classifier_('push', Hstack, Ypush, verbose)
        # fit shift classifier
        self.c_shift_ =  self.fit_classifier_('shift', Hshift, Yshift, verbose)
        # fit output
        if self.classify_output:
            # check for the dimensionality of the output.
            if len(Yout.shape) > 1:
                # If we have more than a single output dimension, fit multiple
                # classifiers
                self.c_out_ = []
                for l in range(Yout.shape[1]):
                    self.c_out_.append(self.fit_classifier_('out_%d' % (l+1), Hout, Yout[:, l], verbose))
                    
                    #proj  = PCA(n_components=2).fit_transform(Hout)
                    #fig = plt.figure()
                    #ax1  = fig.add_subplot(211) # , projection='3d'
                    #plt.plot(proj[Yout[:, l]==0,0], proj[Yout[:, l]==0,1], 'ro')
                    #ax2  = fig.add_subplot(212, sharex = ax1, sharey = ax1)
                    #plt.plot(proj[Yout[:, l]==1,0], proj[Yout[:, l]==1,1], 'bo')
                    #plt.show()
            else:
                # Otherwise fit a single output classifier
                self.c_out_ = self.fit_classifier_('out', Hout, Yout, verbose)
        else:
            if verbose:
                print('apply linear regression for output')
            self.c_out_ = Ridge(alpha = self.regul)
            self.c_out_.fit(Hout, Yout)
            if verbose:
                print('trained output regressor with correlation coefficient %g' % self.c_out_.score(Hout, Yout))

        return self

    def train_iteration_(self, x, h, g, stk, pop, push, shift, Hstack, Hshift, Hout):
        """ Applies one iteration of the reservoir stack machine to the
        training data in order to generate the state vectors for classifiers.

        Parameters
        ----------
        x: ndarray of size self.U.shape[1]
            The current input vector. If this is None, we assume that it's
            the end of the input.
        h: ndarray of size self.U.shape[0]
            The current input state vector.
        g: ndarray of size self.V.shape[0]
            The current stack state vector.
        stk: list
            The current stack. Note that this is changed in place.
        pop: list
            The list of desired pop actions in the current iteration.
        push: list
            The list of desired push actions in the current iteration.
        shift: int
            1 if x should be put on the stack, 0 otherwise.
        Hstack: list
            The list to which training inputs for the pop and push classifier
            should be appended.
        Hout: list
            The list to which training inputs for the shift classifier should
            be appended.
        Hout: list
            The list to which training inputs for the output classifier should
            be appended.
        verbose: bool (default = False)
            If set to True, this function will print verbose messages regarding
            its internal state during computation.

        Returns
        -------
        h: ndarray of size self.U.shape[0]
            The updated input state vector.
        g: ndarray of size self.V.shape[0]
            The updated stack state vector.

        """

        # update the state
        if x is None:
            h = self.nonlin(self.W * h)
        else:
            h = self.nonlin(np.dot(self.U, x) + self.W * h)

        # concatenate with stack state
        hg = np.expand_dims(np.concatenate((h, g)), 0)

        # verify that the length of pop and push agrees
        if len(pop) != len(push):
            raise ValueError('There were %d pop actions but %d push actions. These numbers need to be the same.' % (len(pop), len(push)))

        for j in range(len(pop)):

            # append the current state to the training data
            Hstack.append(hg)
            # if the training data imposes a pop action, apply it
            if pop[j] > 0:
                # apply the pop action
                for r in range(pop[j]):
                    stk.pop()
                # and reset the reservoir stack state
                g = np.zeros(self.V.shape[0])
                for sym in stk:
                    if isinstance(sym, int):
                        g = self.nonlin(self.V[:, sym-1] + self.W * g)
                    else:
                        g = self.nonlin(np.dot(self.U, sym) + self.W * g)
            # apply the push action if so desired
            if push[j] > 0:
                # push nonterminal onto the stack
                stk.append(push[j])
                # update stack state
                g = self.nonlin(self.V[:, push[j]-1] + self.W * g)
            # update hg
            hg = np.expand_dims(np.concatenate((h, g)), 0)

        # end each pop and push action phase with a zero action
        Hstack.append(hg)

        # append output training data
        Hout.append(hg)

        if x is not None:
            # append shift training data
            Hshift.append(hg)
            # apply the shift action if desied
            if shift > 0.5:
                # push input symbol onto the stack
                stk.append(x)
                # update stack state
                g = self.nonlin(np.dot(self.U, x) + self.W * g)

        return h, g


    def predict(self, X, max_inner_loop = None, verbose = False):
        """ Returns the predicted output sequence for the given input sequence.

        Parameters
        ----------
        X: ndarray of size T x n
            The input time series with T time steps and n features where n
            must be equal to self.U.shape[1]
        max_inner_loop: int (default = None)
            To prevent endless loops, we can, as a last resort, stop after a
            fixed number of iterations.
        verbose: bool (default = False)
            If set to True, this function will print verbose messages regarding
            its internal state during computation.


        Returns
        -------
        Y: ndarray of size T + 1 x L
            The output time series with T time steps and L outputs where L is
            equal to self.Wout_.shape[0].

        """
        m, n = self.U.shape
        _, k = self.V.shape

        if self.V.shape[0] != m:
            raise ValueError('Expected %d rows in nonterminal to state matrix, but got %d rows.' % (m, self.V.shape[0]))

        if self.W.shape[0] != m or self.W.shape[1] != m:
            raise ValueError('Expected a %d x %d reservoir matrix W but had a %d x %d matrix.' % (m, m, self.W.shape[0], self.W.shape[1]))

        # check input dimensionality
        if X.shape[1] != n:
            raise ValueError('Expected %d input dimensions but got %d' % (n, X.shape[1]))
        T = X.shape[0]

        # initialize the stack
        stk = []
        # initialize the state
        h = np.zeros(m)
        # initialize the stack state
        g = np.zeros(m)

        # start processing
        Y = []
        for t in range(T):
            if verbose:
                print('--- time step %d ---' % t)

            # do a prediction iteration
            y, h, g, pop, push = self.predict_iteration_(X[t, :], h, g, stk, max_inner_loop, verbose)
            Y.append(y)

        if verbose:
            print('--- end of sequence ---')
        # do a final iteration at the end of the sequence
        y, h, g, pop, push = self.predict_iteration_(None, h, g, stk, max_inner_loop, verbose)
        Y.append(y)

        Y = np.stack(Y, axis = 0)
        return Y

    def predict_iteration_(self, x, h, g, stk, max_inner_loop = None, verbose = False):
        """ Performs a single iteration of the reservoir stack machine model.

        Parameters
        ----------
        x: ndarray of size self.U.shape[1]
            The current input vector. If this is None, we assume that it's
            the end of the input.
        h: ndarray of size self.U.shape[0]
            The current input state vector.
        g: ndarray of size self.V.shape[0]
            The current stack state vector.
        stk: list
            The current stack. Note that this is changed in place.
        max_inner_loop: int (default = None)
            To prevent endless loops, we can, as a last resort, stop after a
            fixed number of iterations.
        verbose: bool (default = False)
            If set to True, this function will print verbose messages regarding
            its internal state during computation.

        Returns
        -------
        y: ndarray of size L
            The current output of self.c_out_.
        h: ndarray of size self.U.shape[0]
            The updated input state vector.
        g: ndarray of size self.V.shape[0]
            The updated stack state vector.
        pops: list
            The current list of pop actions.
        pushs: list
            The current list of push actions.

        """
        if verbose:
            if x is None:
                print('no input')
            else:
                print('current input: %s' % str(x))

        # update the state
        if x is None:
            h = self.nonlin(self.W * h)
        else:
            h = self.nonlin(np.dot(self.U, x) + self.W * h)

        # concatenate input state with stack state
        hg = np.expand_dims(np.concatenate((h, g)), 0)

        # start a loop for reduce rules (i.e. pop and push combinations)
        # until neither is used anymore
        pop = 1
        push = 1

        # store the last push to to prevent endless loops
        last_pop = None
        last_push = None

        # also count the number of inner loops
        num_loops = 0

        pops  = []
        pushs = []

        while pop > 0 or push > 0:
            num_loops += 1
            if max_inner_loop is not None and num_loops > max_inner_loop:
                if verbose:
                    print('Had to break an endless loop')
                break

            if verbose:
                print('current combined state: %s' % str(hg))

                print('current stack: %s' % str(stk))

            # ask the classifiers whether we should pop from and/or push to
            # the stack
            pop = min(self.c_pop_.predict(hg)[0], len(stk))
            push = int(self.c_push_.predict(hg)[0])

            # apply pop
            pops.append(pop)

            if verbose:
                print('pop prediction: %d' % pop)

            if pop > 0:
                # pop elements from the stack
                for i in range(pop):
                    stk.pop()
                # and reset the reservoir stack state
                g = np.zeros(self.V.shape[0])
                for sym in stk:
                    if isinstance(sym, int):
                        g = self.nonlin(self.V[:, sym-1] + self.W * g)
                    else:
                        g = self.nonlin(np.dot(self.U, sym) + self.W * g)

            # apply push
            pushs.append(push)

            if verbose:
                print('push prediction: %d' % push)

            if push > 0:
                # push nonterminal onto the stack
                stk.append(push)
                # update stack state
                g = self.nonlin(self.V[:, push-1] + self.W * g)

            last_push = push
            last_pop  = pop

            # update hg
            hg = np.expand_dims(np.concatenate((h, g)), 0)

        if x is not None:
            # check if we wish to apply a shift rule
            shift = self.c_shift_.predict(hg)[0]

            if verbose:
                print('shift prediction: %d' % shift)

            if shift > 0:
                # push input symbol onto the stack
                stk.append(x)
                # update stack state
                g = self.nonlin(np.dot(self.U, x) + self.W * g)

        # generate output
        if isinstance(self.c_out_, list):
            # if there are multiple output models, let them all predict
            y = np.zeros(len(self.c_out_))
            for l in range(len(self.c_out_)):
                y[l] = self.c_out_[l].predict(hg)
        else:
            y = self.c_out_.predict(hg).squeeze()

        if verbose:
            print('current combined state: %s' % str(hg))
            print('current stack: %s' % str(stk))
            print('output prediction: %s' % str(y))

        return y, h, g, pops, pushs


def one_hot_coding(strings, alphabet = None):
    """ Convers the input strings to one-hot-coding matrices using the
    order of characters in the given alphabet.

    Parameters
    ----------
    strings: list
        A list of strings
    alphabet: list (default = None)
        A list of characters. If not given this is inferred from the data.

    Returns
    -------
    Xs: list
        A list of one-hot codings, one per input string.

    """
    Xs = []

    # establish alphabet if not given
    if alphabet is None:
        alphabet = set()
        for word in strings:
            for x in word:
                alphabet.add(x)
        alphabet = list(sorted(alphabet))

    n = len(alphabet)

    # generate a mapping from symbols to indices
    idxs = {}
    c = 0
    for x in alphabet:
        idxs[x] = c
        c += 1

    # convert inputs
    for word in strings:
        X = np.zeros((len(word), n))
        for t in range(len(word)):
            c = idxs[word[t]]
            X[t, c] = 1.
        Xs.append(X)

    return Xs


def lr_to_training_data(parser, strings, alphabet = None):
    """ Converts a list of strings to training data for a reservoir stack
    machine using the given LR(1) parser.

    Parameters
    ----------
    parser: class cfg_utils.LRParser
        A LR(1) parser for the language.
    strings: list
        A list of strings that roughly cover the language.
        TODO: describe a minimal set automatically.
    alphabet: list (default = None)
        A list of characters. If not given this is inferred from the data.

    Returns
    -------
    Xs: list of ndarrays of size T x n
        One input time series per string where each character is one-hot coded.
    pops: list of lists
        One time series of desired pop actions for each string.
    pushs: list of lists
        One time series of desired push actions for each string.
    shifts: ndarray of length T
        One time series of desired shift actions for each string
        (will be constant 1).
    Ys: ndarray of length T
        One binary time series where Ys[i][t] is one if strings[i][:t] is a
        valid string according to the parser.

    """
    # one-hot-code the inputs
    Xs = one_hot_coding(strings, alphabet)

    # construct a mapping from nonterminals to indices
    alphabet = set()
    for rule in parser.rules:
        alphabet.add(rule[3])
    idxs_nonts = {}
    c = 1
    for x in list(sorted(alphabet)):
        idxs_nonts[x] = c
        c += 1

    # initialize output
    pops   = []
    pushs  = []
    shifts = []
    Ys     = []

    # start converting the input strings
    for word in strings:
        # parse the current word using the LR parser
        rules, y = parser.parse(word)
        # generate list of pop and push actions
        pop  = []
        push = []
        for rule_list in rules:
            pop_list = []
            push_list = []
            for rule in rule_list:
                pop_list.append(rule[2])
                push_list.append(idxs_nonts[rule[3]])
            pop.append(pop_list)
            push.append(push_list)
        # generate shift list
        shift = np.full(len(word), 1.)
        # append all to training data
        pops.append(pop)
        pushs.append(push)
        shifts.append(shift)
        Ys.append(y)
    # return
    return Xs, pops, pushs, shifts, Ys
