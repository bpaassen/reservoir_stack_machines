# Reservoir Stack Machines

Copyright (C) 2020 - Benjamin Paassen and Alexander Schulz
The University of Sydney

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

## Introduction

_Reservoir stack machines_ (RSMs) are a combination of Echo State Networks
(ESNs, [Jaeger and Haas, 2004][ESN]) with a stack for memory, simulating an
LR(1)-automaton ([Knuth, 1965][LR]). This repository contains a reference
implementation of RSMs, accompanying the paper

* Paaßen, Schulz, and Hammer (2020). Reservoir Stack Machines.
  Neurocomputing, in press. doi:[10.1016/j.neucom.2021.05.106](https://doi.org/10.1016/j.neucom.2021.05.106).
  [Link][paper]

Please cite this paper if you use RSMs in academic work.

If you want to reproduce the results from the paper, please refer to the
`experiments.ipynb` and `deep_experiments.ipynb` notebooks. Full reference
results are given in the `results` directory. Test and training data are
given in the `data` directory.

## Quickstart Guide

If you wish to train your own RSM, you first need to collect training
data in the form of an input matrix `X` of size T x n, a list of
desired `pop` actions for each time step, a list of desired `push`
actions for each time step, a vector `shift` containing the desired
shift action for each time step, and a matrix `Y` of size T+1 x L
containing the desired output for each time step.

For example, assume you wish to train a RSM that recognizes the language
$`a^nb^n`$. An example word in that language is aabb. In one-hot-coding,
the this input can be represented as `[[1, 0], [1, 0], [0, 1], [0, 1]]`.
To parse this word, we need to shift the first two letters onto the stack,
then push a nonterminal `S` onto the stack, and then shift, replace the
stack head `aSb` with `S`, shift again, replace again, and terminate.
In other words, for the first two letters we have no pop or push action.
After the second letter, we pop nothing but push `S`, which is the first
(and only) nonterminal of our language. And after the third and fourth
letter we pop three elements from the stack and push `S` back onto it
each time, yielding the following `pop` and `push` lists:

```python
pop  = [[], [], [0], [3], [3]]
push = [[], [], [1], [1], [1]]
```

The desired output is `[0, 0, 0, 0, 1]` because the word is only valid
after the last letter. 

Additionally, we need to set up a reservoir for the model and define the
nonlinearity. In this example, we use Gaussian random numbers.

Overall, we obtain the following code for training our RSM:

```python
import numpy as np
import esn
import rsm

# initialize a reservoir with 32 neurons and 3 inputs for
# two terminal symbols + one nonterminal symbol
U, W  = esn.initialize_reservoir(m = 32, n = 3, radius = 0.95, sparsity = 0.5)
# distribute U into matrix for terminal and matrix for nonterminal symbols
V = U[:, 2:]
U = U[:, :2]

# initialize model
model = rsm.RSM(U, V, W, nonlin = np.tanh)

# prepare training data
X     = np.array([[1, 0], [1, 0], [0, 1], [0, 1]])
pop   = [[], [], [0], [3], [3]]
push  = [[], [], [1], [1], [1]]
shift = np.array([1, 1, 1, 1])
Y     = np.expand_dims(np.array([0, 0, 0, 0, 1]), 1)

model.fit(X, pop, push, shift, Y)
```

The prediction on a new sequence, then, doesn't need reference data for pop,
push, or shift actions anymore; only the input.

```python
Y = model.predict(X)
print(Y)
```

## Contents

This repository contains the following files.

* `anbn_language.py` : An implementation of the language $`a^nb^n`$.
* `cfg_utils.py` : A utility module for context free grammars and LR(1)-automata.
* `cfg_utils_test.py` : Unit tests for `cfg_utils.py`.
* `copy_task.py` : An implementation of the copy task.
* `copy_task_test.py` : Unit tests for `copy_task.py`.
* `crj.py` : An implementation of cycle reservoir with jumps ([Rodan and Tiňo, 2012][CRJ])
* `data` : A directory of the training data used in all experiments.
* `deep_experiments.ipynb` : A notebook containing the deep learning experiments.
* `deep_stack_machines.py` : A [pyTorch][torch] implementation of the reservoir stack machine, replacing the reservoir with a gated recurrent unit (GRU; [Cho et al., 2014][GRU])
* `deep_stack_machine_test.py` : Unit tests for `deep_stack_machine.py`
* `dyck_languages.py` : An implementation of the Dyck languages.
* `esn.py` : An implementation of echo state networks ([Jaeger and Haas, 2004][ESN]).
* `experiments.ipynb` : A notebook containing the shallow learning experiments.
* `json_language.py` : An implementation of a simplified version of the [JSON](https://json.org) language
* `json_language_test.py` : Unit tests for `json_language.py`.
* `LICENSE.md` : A copy of the [GPLv3][GPLv3].
* `lmu.py` : An implementation of Legendre delay networks ([Voelker et al. (2019)][LMU]).
* `palindrome_language.py` : An implementation of the language of palindromes over a and b with $ in the middle.
* `README.md` : This file.
* `repeat_copy_task.py` : An implementation of the repeat copy task.
* `repeat_copy_task_test.py` : Unit tests for `repeat_copy_task.py`.
* `results` : All reference results from the experiments described in the paper.
* `rsm.py` : The reference implementation of Reservoir Memory Networks (RSMs).
* `rsm_test.py` : Unit tests for `rsm.py`.
* `simple_gru.py` : A slightly simplified API for the [pyTorch][torch] implementation of gated recurrent units (GRUs; [Cho et al., 2014][GRU])
* `simple_gru_test.py` : Unit tests for `simple_gru.py`.
* `stack_rnn.py` : A slightly simplified API for the `rnn_model.py` implementation of stack-RNNs by [Suzgun et al. (2019)][SRNN].
* `stack_rnn_test.py` : Unit tests for `stack_rnn.py`.

## Licensing

This library is licensed under the [GNU General Public License Version 3][GPLv3].
This documentation is licensed under [CC-BY-SA 4.0][CCBYSA].

## Dependencies

This library depends on [NumPy][np] for matrix operations, on [scikit-learn][sklearn]
for the base interfaces and on [SciPy][scipy] for optimization. Further, if you
wish to reproduce the deep learning results, you need to download the file
`rnn_models.py` from the [stack-RNN reference implementation](https://github.com/suzgunmirac/marnns/blob/master/models/rnn_models.py)
and put it in this folder here.

## Literature

* Paaßen, Schulz, and Hammer (2020). Reservoir Stack Machines. Neurocomputing, in press.  doi:[10.1016/j.neucom.2021.05.106](https://doi.org/10.1016/j.neucom.2021.05.106). [Link][paper]
* Jaeger and Haas (2004). Harnessing nonlinearity: Predicting chaotic systems and saving energy in wireless communication. Science, 304(5667), 78-80. doi:[10.1126/science.1091277][ESN]
* Knuth (1965). On the translation of languages from left to right. Information and Control, 8(6), 607-639. doi:[10.1016/S0019-9958(65)90426-2][LR]
* Rodan and Tiňo (2012). Simple Deterministically Constructed Cycle Reservoirs with Regular Jumps. Neural Compuation, 24(7), 1822-1852. doi:[10.1162/NECO_a_00297][CRJ]
* Voelker, Kajić, and Eliasmith (2019). Legendre Memory Units: Continuous-Time Representation in Recurrent Neural Networks. In: Wallach, Larochelle, Beygelzimer, d'Alché-Buc, Fox, and Garnett. (eds.). Proceedings of the 32nd International Conference on Advances in Neural Information Processing Systems 32 (NeurIPS 2019), 15570--15579. [Link][LMU]
* Cho, van Merrienboer, Gulcehre, Bahdanau, Bougares, Schwenk, and Bengio (2014). Learning Phrase Representations using RNN Encoder-Decoder for Statistical Machine Translation. Proceedings of the 2014 Conference on Empirical Methods in Natural Language Processing (EMNLP 2014), 1724-1734. [Link][GRU]
* Suzgun, Gehrmann, Belinkov, and Shieber (2019). Memory-Augmented Recurrent Neural Networks Can Learn Generalized Dyck Languages. arXiv:[1911.03329][SRNN]

[GPLv3]: https://www.gnu.org/licenses/gpl-3.0.en.html "The GNU General Public License Version 3"
[CCBYSA]:https://creativecommons.org/licenses/by-sa/4.0/ "Creative Commons Attribution 4.0 International (CC BY SA 4.0) License"
[np]: http://numpy.org/ "Numpy"
[sklearn]: https://scikit-learn.org/stable/ "Scikit-learn homepage"
[scipy]: https://scipy.org/ "SciPy homepage"
[torch]:https://pytorch.org/ "pyTorch"
[ESN]:https://doi.org/10.1126/science.1091277 "Jaeger and Haas (2004). Harnessing nonlinearity: Predicting chaotic systems and saving energy in wireless communication. Science, 304(5667), 78-80. doi:10.1126/science.1091277"
[LR]:https://doi.org/10.1016/S0019-9958(65)90426-2 "Knuth (1965). On the translation of languages from left to right. Information and Control, 8(6), 607-639. doi:10.1016/S0019-9958(65)90426-2"
[CRJ]:https://doi.org/10.1162/NECO_a_00297 "Rodan and Tino (2012). Simple Deterministically Constructed Cycle Reservoirs with Regular Jumps. Neural Compuation, 24(7), 1822-1852. doi:10.1162/NECO_a_00297"
[LMU]:http://papers.nips.cc/paper/9689-legendre-memory-units-continuous-time-representation-in-recurrent-neural-networks "Voelker, Kajić, and Eliasmith (2019). Legendre Memory Units: Continuous-Time Representation in Recurrent Neural Networks. In: Wallach, Larochelle, Beygelzimer, d'Alché-Buc, Fox, and Garnett. (eds.). Proceedings of the 32nd International Conference on Advances in Neural Information Processing Systems 32 (NeurIPS 2019), 15570--15579."
[GRU]:http://www.emnlp2014.org/papers/pdf/EMNLP2014179.pdf "Cho, van Merrienboer, Gulcehre, Bahdanau, Bougares, Schwenk, and Bengio (2014). Learning Phrase Representations using RNN Encoder-Decoder for Statistical Machine Translation. Proceedings of the 2014 Conference on Empirical Methods in Natural Language Processing (EMNLP 2014), 1724-1734."
[SRNN]:https://arxiv.org/abs/1911.03329 "Suzgun, Gehrmann, Belinkov, and Shieber (2019). Memory-Augmented Recurrent Neural Networks Can Learn Generalized Dyck Languages. arXiv:1911.03329"
[paper]:https://arxiv.org/abs/2105.01616v2 "Paaßen, Schulz, and Hammer (2020). Reservoir Stack Machines. Neurocomputing, in press. doi:10.1016/j.neucom.2021.05.106"