"""
Implements the language a^nb^n.

"""

# Copyright (C) 2020
# Alexander Schulz and Benjamin Paaßen
# Bielefeld University and The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Alexander Schulz and Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__Version__ = '0.0.1'
__maintainer__ = 'Alexander Schulz and Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

from collections import deque
import numpy as np
import cfg_utils

# implement CFG for the a^nb^n language
anbn_GRAMMAR = cfg_utils.CFGGrammar({'S' : ['aSb', 'ab']}, 'S')



# implement LR(1)-Parser for the a^n b^n language
anbn_PARSER = cfg_utils.LRParser([
            ('aSb', '', 3, 'S'),
            ('ab', '', 2, 'S')
        ], 'S')

